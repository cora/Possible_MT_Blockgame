local mod_name = minetest.get_current_modname()
local mod_path = minetest.get_modpath(mod_name)

pmb_fire = {}

dofile(mod_path .. DIR_DELIM .. "items.lua")
dofile(mod_path .. DIR_DELIM .. "nodes.lua")
dofile(mod_path .. DIR_DELIM .. "abms.lua")
