local mod_name = minetest.get_current_modname()
local S = minetest.get_translator(mod_name)


local function put_fire(itemstack, user, pointed_thing)
  if (not pointed_thing) or not pointed_thing.under then return end
  local to_node = minetest.registered_nodes[minetest.get_node(pointed_thing.under).name]
  if to_node and to_node.walkable and not to_node.buildable_to then
    local firenode = minetest.get_node(pointed_thing.above)
    if (not firenode) or minetest.registered_nodes[firenode.name].buildable_to then
      minetest.set_node(pointed_thing.above, {name="pmb_fire:fire"})
      itemstack:add_wear(65536/90)
    end
  end
  return itemstack
end

minetest.register_tool("pmb_fire:flint_and_steel", {
	description = S("Flint and steel"),
	inventory_image = "pmb_flint_and_steel.png",
	stack_max = 1,
	groups = { usable = 1, starts_fires = 1 },
	on_place = put_fire,
  uses = 30,
})

minetest.register_craft({
  output = "pmb_fire:flint_and_steel",
  type = "shapeless",
  recipe = {
    "pmb_items:iron_bar", "pmb_stone:cobble",
  },
})