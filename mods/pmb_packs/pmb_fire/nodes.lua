
pmb_fire = {}

local fire_size = {
  type = "fixed",
  fixed = {-4/16, -8/16, -4/16, 4/16, 3/16, 4/16},}

minetest.register_node('pmb_fire:fire', {
  description = 'Just fire.',
  groups = { item_fire = 1, oddly_breakable_by_hand = 1, fire = 1, lights_fires = 1, spreads_fire = 1, },
	drawtype = "mesh",
  -- drawtype = "nodebox",
  -- node_box = {
  --   type = "fixed",
  --   fixed = {
  --     -9/16, -25/16, -9/16,
  --     9/16, -7/16, 9/16,
  --   },
  -- },
	tiles = {{
			name = "pmb_fire.png", -- this is the flame texture
			animation = {
				type = "vertical_frames",
				aspect_w = 16,
				aspect_h = 16,
				length = 0.5,
			},},},
  drop = "",
  selection_box = fire_size,
  walkable = false,
  -- pointable = false,
  buildable_to = true,
	mesh = "pmb_fire.b3d",
  light_source = 12,
  sounds = (pmb_sounds.default and pmb_sounds.default_stone()),
	collision_box = nil,
  on_punch = function(pos, node, clicker, itemstack, pointed_thing)
    minetest.dig_node(pos)
  end,
})


function pmb_fire.spread(pos, node)
  if math.random() < 0.1
  or (minetest.find_node_near(pos, 0.5, {"group:fire"}) and math.random() < 0.9) then -- random chance to go out
    minetest.dig_node(pos)
    return
  end
  local nodes = minetest.find_nodes_in_area_under_air(
    vector.offset(pos, -1, -1, -1),
    vector.offset(pos,  1,  1,  1),
    {"group:flammable"})
  local spread_pos = nil
  for noderef, value in pairs(nodes) do
    if true then
      spread_pos = value
      break
    end
  end
  if not spread_pos then
    return
  end
  spread_pos = vector.offset(spread_pos, 0, 1, 0)
  if minetest.get_node(spread_pos).name == "air" then
    minetest.set_node(spread_pos, {name=node.name})
  end
end

-- spread fire
minetest.register_abm({
  nodenames = {"group:spreads_fire"},
  neighbours = {"group:flammable"},
  interval = 1.0,
  chance = 10,
  action = pmb_fire.spread,
})

-- burn wood down or disappear
minetest.register_abm({
  nodenames = {"pmb_fire:fire"},
  neighbours = {"group:flammable"},
  interval = 1.0,
  chance = 20,
  action = function(pos, node)
    local below = minetest.get_node(vector.offset(pos, 0, -1, 0))
    local is_above_flammable = (minetest.get_item_group(below.name, "flammable") ~= 0)
    if not is_above_flammable then
      minetest.dig_node(pos)
    elseif is_above_flammable then
      if below.name == "pmb_wood:hot_coals" and math.random() < 0.6 then
        minetest.set_node(vector.offset(pos, 0, -1, 0), {name = "pmb_fire:fire"})
        minetest.dig_node(pos)
      else
        minetest.set_node(vector.offset(pos, 0, -1, 0), {name = "pmb_wood:hot_coals"}) -- pmb_wood:charcoal_block
      end
    end
  end,
})
