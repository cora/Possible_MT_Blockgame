

function pmb_cooking.do_node_sound(pos, node, sound)
  local index = tostring(pos)
  if (not node_sounds[index]) or node_sounds[index].timer > 10 then
    node_sounds[index] = {handle = 0, timer = 0}
    node = minetest.registered_nodes[node.name]
    if type(sound) ~= "table" then
      if node._custom_sounds then
        sound = node._custom_sounds.passive
      else
        sound = {}
      end
    end
    local name = sound.name
    sound.pos = pos

    node_sounds[index].handle = minetest.sound_play(name, sound)
  end
end
function pmb_cooking.stop_node_sound(pos)
  local index = tostring(pos)
  if node_sounds[index]
  and node_sounds[index].handle then
    minetest.sound_stop(node_sounds[index].handle)
  end
  node_sounds[index] = nil
end
-- ABM for playing node sounds
minetest.register_abm({
  nodenames = {'group:cooker_sound'},
  interval = 1.0,
  chance = 3,
  action = pmb_cooking.do_node_sound,
})

function pmb_cooking.node_sound_step(dtime)
  for index, sound in pairs(node_sounds) do
    sound.timer = sound.timer + dtime
    if sound.timer > 10 then
      sound = nil
    end
  end
end

minetest.register_globalstep(pmb_cooking.node_sound_step)
