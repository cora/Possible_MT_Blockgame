
pmb_cooking.register_cooking({
  raw = "pmb_wood:oak_log",
  cooked = "pmb_wood:ash_log", -- hah, get it? ash.
  input_count = 1, -- optional or 1
  output_count = 1, -- optional or 1
  cook_time = 2, -- optional or 5, how long it takes to cook (if not instant). -1 for instant.
  groups = {"cooker_boil", "cooker_roast"},
})
pmb_cooking.register_cooking({
  raw = "pmb_wood:ash_log",
  cooked = "pmb_wood:oak_log", -- and you can uncook it too
  input_count = 1, -- optional or 1
  output_count = 1, -- optional or 1
  cook_time = 2, -- optional or 5, how long it takes to cook (if not instant). -1 for instant.
  groups = {"cooker_boil"},
})
