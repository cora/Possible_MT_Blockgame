
local pot_size = {
  type = "fixed",
  fixed = {-4/16, -8/16, -4/16, 4/16, -2/16, 4/16},
}

function pmb_cooking.pot_give_item(pos, node, clicker, itemstack, pointed_thing)
  minetest.get_node_timer(pos):start(1.0)
  local self = minetest.registered_nodes[node.name]
  if not pmb_cooking.get_recipe(itemstack:get_name(), self._cook_group(pos)) then
    return itemstack
  end

  local meta = minetest.get_meta(pos)
  local inv = meta:get_inventory()


  local result = inv:add_item("input", ItemStack(itemstack:get_name()))
  if not result:is_empty() then
    return itemstack
  else
    itemstack:take_item()
    return itemstack
  end
end

-- on punch
function pmb_cooking.pot_take_item(pos, node, clicker, itemstack, pointed_thing)
  local meta = minetest.get_meta(pos)
  local inv = meta:get_inventory()
  local output = inv:get_stack('output', 1)
  local input = inv:get_stack('input', 1)

  -- take stuff from the inv and give to player
  local clicker_inv = clicker:get_inventory()
  if not inv:is_empty('output') then
    output = clicker_inv:add_item('main', output)
    inv:set_stack("output", 1, output)
    return itemstack
  elseif not inv:is_empty('input') then
    input = clicker_inv:add_item('main', input)
    inv:set_stack("input", 1, input)
    return itemstack
  end
end


-- happens every second
function pmb_cooking.pot_node_timer(pos, elapsed)
  local node = minetest.registered_nodes[minetest.get_node(pos).name]
  local cook_group
  if type(node._cook_group) == "function" then
    cook_group = node._cook_group(pos)
  end

  -- minetest.log("thinking")
  local meta = minetest.get_meta(pos)
	local inv = meta:get_inventory()


  -- don't cook if there's no heat
  if not cook_group then return end

  local has_cooked = false

  local itemstack = inv:get_stack("input", 1)
  if not itemstack then return false end
  local cooked

  local recipe = pmb_cooking.get_recipe(itemstack:get_name(), cook_group)
	local timer_elapsed = meta:get_int("timer_elapsed") or 0

  if recipe then
    meta:set_int("timer_elapsed", timer_elapsed + 1)
  end

  if (not recipe or not recipe.cook_time)
  or (timer_elapsed < recipe.cook_time) then return true
  else
    meta:set_int("timer_elapsed", 0)
  end

  itemstack, cooked = pmb_cooking.cook(itemstack, cook_group, 1)

  if cooked and inv:room_for_item("output", cooked) then
    has_cooked = true
  end

  if has_cooked then
    inv:add_item("output", cooked)
    inv:set_stack("input", 1, itemstack)
    if itemstack:get_count() == 0 then
      minetest.sound_play("pmb_cooking_finished_all", {
        gain = 0.5,
        pos = pos,
        max_hear_distance = 5,
      })
    end
    node._on_cook(pos, node)
  end

  return true
end

function pmb_cooking.pot_on_construct(pos)
  local meta = minetest.get_meta(pos)
  local inv = meta:get_inventory()
  inv:set_size('input', 1)
  inv:set_size('output', 1)
  -- pmb_cooking.pot_node_timer(pos, 0)
  minetest.get_node_timer(pos):start(1.0)
end

minetest.register_node('pmb_cooking:pot', {
  description = 'A cast iron cooking pot',
  groups = { item_cooking_pot = 1, solid = 1, oddly_breakable_by_hand = 2, iron = 1, cooker = 1, cooker_boil = 1 },
	drawtype = "mesh",
  paramtype = "light",
  paramtype2 = "facedir",
	tiles = {"pmb_cooking_pot.png"},
	mesh = "pmb_cooking_pot.b3d",
  sounds = (pmb_sounds and pmb_sounds.default and pmb_sounds.default()) or {},
	collision_box = pot_size,
	selection_box = pot_size,
  _on_node_update = function(pos, cause, user, count)
    minetest.get_node_timer(pos):start(1.0)
  end,
  _on_cook = function(pos, node)
    for x=0, 10 do
      minetest.add_particle({
        pos = vector.offset(pos, math.random()-0.5, math.random()-0.5, math.random()-0.5),
        velocity = vector.new(0,math.random(),0),
        expirationtime = math.random() * 4,
        size = math.random() * 4,
        collisiondetection = false,
        vertical = false,
        texture = "pmb_cooking_particle_finished_cooking.png^[colorize:#ffffff55:200",
        glow = 14,
      })
      minetest.sound_play("pmb_cooking_finished_boil", {
        gain = 0.05,
        pos = pos,
        max_hear_distance = 5,
      })
    end
  end,
  _cook_group = function(pos)
    local below = minetest.get_node(vector.offset(pos, 0, -1, 0))
    if  minetest.get_item_group(below.name, "fire") ~= 0 then
      return "cooker_boil"
    elseif minetest.get_item_group(below.name, "melter") ~= 0
    or minetest.get_item_group(below.name, "lava") ~= 0 then
      return "cooker_crucible"
    else
      return nil
    end
  end,
	on_timer = pmb_cooking.pot_node_timer,
  on_construct = pmb_cooking.pot_on_construct,
  on_punch = function(pos, node, clicker, itemstack, pointed_thing)
    pmb_cooking.pot_take_item(pos, node, clicker, itemstack, pointed_thing)
  end,
  on_rightclick = function(pos, node, clicker, itemstack, pointed_thing)
    return pmb_cooking.pot_give_item(pos, node, clicker, itemstack, pointed_thing)
  end,
  after_place_node = function(pos, placer, itemstack, pointed_thing)
    return pmb_util.rotate_and_place_from(pos, placer, itemstack, pointed_thing, {offset = 0, copy_same_node = false})
  end,
})




-- temporary crafting recipe
if true then
  local s = "pmb_items:iron_nugget"
  minetest.register_craft({
    output = "pmb_cooking:pot",
    recipe = {
      {s, "", s},
      {s, "", s},
      {s,  s, s},
    },
  })
end


