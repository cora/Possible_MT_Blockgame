

local furnace_size = {
  type = "fixed",
  fixed = {-8/16, -8/16, -8/16, 8/16, 8/16, 8/16},}

minetest.register_node('pmb_cooking:furnace', {
  description = 'A pit for fires.',
  groups = { item_furnace = 1, solid = 1, oddly_breakable_by_hand = 3, stone = 1, },
	drawtype = "mesh",
	tiles = {
    "pmb_furnace.png^[colorize:#222222:40", -- make the base not as glowy
    {
			name = "pmb_furnace_fire_off.png", -- this is the flame texture
			-- animation = {
			-- 	type = "vertical_frames",
			-- 	aspect_w = 16,
			-- 	aspect_h = 16,
			-- 	length = 0.5,
			-- },
		},},
	mesh = "pmb_furnace.b3d",
  sounds = (pmb_sounds and pmb_sounds.default_stone and pmb_sounds.default_stone()) or {},
  _custom_sounds = {
    passive = {
      name = "pmb_furnace_fire_cc0",
      max_hear_distance = 5,
      gain = 0.7,
    },
  },
	collision_box = furnace_size,
	selection_box = furnace_size,
  on_rightclick = pmb_cooking.activate_burner,
  paramtype2 = "facedir",
  after_place_node = function(pos, placer, itemstack, pointed_thing)
    return pmb_util.rotate_and_place_from(pos, placer, itemstack, pointed_thing, {offset = 0, copy_same_node = false})
  end,
})

-- spawn particles
minetest.register_abm({
  nodenames = {'pmb_cooking:furnace_active'},
  interval = 1.0,
  chance = 1,
  action = function(pos)
    for i=0, 4 do
      minetest.add_particle({
        pos = vector.offset(pos, math.random()-0.5, math.random(), math.random()-0.5),
        velocity = vector.new(0,math.random(),0),
        expirationtime = math.random() * 4,
        size = math.random() * 8,
        collisiondetection = false,
        vertical = false,
        texture = "pmb_cooking_particle_finished_cooking.png^[colorize:#44444400:100",
        glow = 14,
      })
      minetest.add_particle({
        pos = vector.offset(pos, math.random()-0.5, math.random(), math.random()-0.5),
        velocity = vector.new(0,math.random()*3,0),
        expirationtime = math.random() * 1,
        size = math.random()+0.1,
        collisiondetection = false,
        vertical = false,
        texture = "pmb_cooking_particle_finished_cooking.png^[colorize:#ffee8855:200",
        glow = 14,
      })
    end
  end,
})


minetest.register_node('pmb_cooking:furnace_active', {
  description = 'A pit for fires. Wait how are you carrying around a burning fireplace in your pocket?',
  groups = { item_furnace = 1, solid = 1, oddly_breakable_by_hand = 3, stone = 1, melter = 1, cooker_sound = 1, },
	drawtype = "mesh",
	tiles = {
    "pmb_furnace.png^[colorize:#222222:40", -- make the base not as glowy
    {
			name = "pmb_furnace_fire_on.png", -- this is the flame texture
			animation = {
				type = "vertical_frames",
				aspect_w = 8,
				aspect_h = 14,
				length = 1,
			},
		},},
  drop = "pmb_cooking:furnace",
	mesh = "pmb_furnace.b3d",
  light_source = 12,
  sounds = (pmb_sounds and pmb_sounds.default_stone and pmb_sounds.default_stone()) or {},
  _fuel_use = 2,
  _custom_sounds = {
    passive = {
      name = "pmb_furnace_fire_cc0",
      max_hear_distance = 5,
      gain = 0.7,
    },
  },
	collision_box = furnace_size,
	selection_box = furnace_size,
  paramtype2 = "facedir",
  on_timer = pmb_cooking.burner_on_timer,
  after_place_node = function(pos, placer, itemstack, pointed_thing)
    return pmb_util.rotate_and_place_from(pos, placer, itemstack, pointed_thing, {offset = 0, copy_same_node = false})
  end,
  on_rightclick = function(pos, node, clicker, itemstack, pointed_thing)
    pmb_cooking.activate_burner(pos, node, clicker, itemstack, pointed_thing)
  end,
  on_destruct = function(pos)
    pmb_cooking.stop_node_sound(pos)
  end
})




if true then
  local s = "pmb_stone:cobble"
  minetest.register_craft({
    output = "pmb_cooking:furnace",
    recipe = {
      {s,  s, s},
      {s, "", s},
      {s,  s, s},
    },
  })
end

