

local fire_pit_size = {
  type = "fixed",
  fixed = {-8/16, -8/16, -8/16, 8/16, 0, 8/16},}

minetest.register_node('pmb_cooking:fire_pit', {
  description = 'A pit for fires.',
  groups = { item_fire_pit = 1, solid = 1, oddly_breakable_by_hand = 2, stone = 1, },
	drawtype = "mesh",
	tiles = {"pmb_fire_pit.png",},
	mesh = "pmb_fire_pit.b3d",
  sounds = (pmb_sounds and pmb_sounds.default_stone and pmb_sounds.default_stone()) or {},
	collision_box = fire_pit_size,
	selection_box = fire_pit_size,
  on_rightclick = pmb_cooking.activate_burner,
  _custom_sounds = {
    passive = {
      name = "pmb_fire_cc0",
      max_hear_distance = 5,
      gain = 0.3,
    },
  },
})

minetest.register_abm({
  nodenames = {'pmb_cooking:fire_pit_active'},
  interval = 1.0,
  chance = 1,
  action = function(pos)
    for i=0, 4 do
      minetest.add_particle({
        pos = vector.offset(pos, math.random()-0.5, math.random()-0.5, math.random()-0.5),
        velocity = vector.new(0,math.random(),0),
        expirationtime = math.random() * 4,
        size = math.random() * 8,
        collisiondetection = false,
        vertical = false,
        texture = "pmb_cooking_particle_finished_cooking.png^[colorize:#44444400:100",
        glow = 14,
      })
      minetest.add_particle({
        pos = vector.offset(pos, math.random()-0.5, math.random()-0.5, math.random()-0.5),
        velocity = vector.new(0,math.random()*3,0),
        expirationtime = math.random() * 1,
        size = math.random()+0.1,
        collisiondetection = false,
        vertical = false,
        texture = "pmb_cooking_particle_finished_cooking.png^[colorize:#ffee8855:200",
        glow = 14,
      })
    end
  end,
})


minetest.register_node('pmb_cooking:fire_pit_active', {
  description = 'A pit for fires. Wait how are you carrying around a burning fireplace in your pocket?',
  groups = { item_fire_pit = 1, solid = 1, oddly_breakable_by_hand = 2, stone = 1, cooker = 1, cooker_roast = 1, fire = 1, cooker_sound = 1, },
	drawtype = "mesh",
	tiles = {
    "pmb_fire_pit.png^[colorize:#222222:40", -- make the base not as glowy
    {
			name = "pmb_fire.png", -- this is the flame texture
			animation = {
				type = "vertical_frames",
				aspect_w = 16,
				aspect_h = 16,
				length = 0.5,
			},
		},
    "pmb_fire_pit.png",}, -- this is the wood texture
  drop = "pmb_cooking:fire_pit",
	mesh = "pmb_fire_pit_active.b3d",
  light_source = 12,
  sounds = (pmb_sounds and pmb_sounds.default_stone and pmb_sounds.default_stone()) or {},
  _fuel_use = 1,
  _custom_sounds = {
    passive = {
      name = "pmb_fire_cc0",
      max_hear_distance = 5,
      gain = 0.3,
    },
  },
	collision_box = fire_pit_size,
	selection_box = fire_pit_size,
  on_timer = pmb_cooking.burner_on_timer,
  on_rightclick = function(pos, node, clicker, itemstack, pointed_thing)
    pmb_cooking.activate_burner(pos, node, clicker, itemstack, pointed_thing)
  end,
  on_destruct = function(pos)
    pmb_cooking.stop_node_sound(pos)
  end
})


if true then
  local s = "pmb_stone:cobble"
  local w = "group:wood_log"
  minetest.register_craft({
    output = "pmb_cooking:fire_pit",
    recipe = {
      {s,  w, s},
      {s,  s, s},
    },
  })
end
