

function pmb_cooking.activate_burner(pos, node, clicker, itemstack, pointed_thing)
  local wielditem = clicker:get_wielded_item():get_name()

  -- figure out if this node is active or not
  local active_name, inactive_name
  if string.find(node.name, "_active") then
    active_name = node.name
    inactive_name = string.gsub(node.name, "_active", "")
  else
    inactive_name = node.name
    active_name = node.name .. "_active"
  end

  if minetest.get_item_group(wielditem, "fuel") ~= 0 then
    if node.name == inactive_name then
      pmb_cooking.stop_node_sound(pos)
      pmb_cooking.do_node_sound(pos, node)
      node.name = node.name.."_active"
      minetest.set_node(pos, node)
      pmb_node_update.update_node_propagate(pos, "interaction", clicker, 13)
    end

    local t = minetest.get_node_timer(pos)
    local fuel_time = minetest.get_item_group(wielditem, "fuel")
    local meta = minetest.get_meta(pos)
    if fuel_time == 0 then fuel_time = 5 end
    local timer_elapsed = meta:get_int("time_left") or 0
    meta:set_int("time_left", timer_elapsed + fuel_time)

    t:stop()
    t:start(1)

    minetest.sound_play("pmb_burner_add_fuel", {
      gain = 0.5,
      pos = pos,
      max_hear_distance = 5,
    })

    itemstack:take_item()
  elseif wielditem == "" and node.name == active_name then
    pmb_cooking.deactivate_burner(pos, node)
  end
end

function pmb_cooking.burner_on_timer(pos, elapsed)
  local meta = minetest.get_meta(pos)
  local timer_elapsed = meta:get_int("time_left") or 0
  local fuel_use_mult = minetest.registered_nodes[minetest.get_node(pos).name]._fuel_use
  if not fuel_use_mult then fuel_use_mult = 1 end

  meta:set_int("time_left", timer_elapsed - fuel_use_mult)
  if timer_elapsed < 0 then
    pmb_cooking.deactivate_burner(pos, minetest.get_node(pos))
    return false
  else
    return true
  end
end

function pmb_cooking.deactivate_burner(pos, node)
  node.name = string.gsub(node.name, "_active", "")
  minetest.set_node(pos, node)
  pmb_cooking.stop_node_sound(pos)
  return false
end
