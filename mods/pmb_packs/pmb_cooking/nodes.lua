local mod_name = minetest.get_current_modname()
local mod_path = minetest.get_modpath(mod_name)

node_sounds = {}


dofile(mod_path .. DIR_DELIM .. "nodes" .. DIR_DELIM .. "cooking_pot.lua")
dofile(mod_path .. DIR_DELIM .. "nodes" .. DIR_DELIM .. "fire_pit.lua")
dofile(mod_path .. DIR_DELIM .. "nodes" .. DIR_DELIM .. "furnace.lua")

