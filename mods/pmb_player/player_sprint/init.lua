
player_sprint = {}

function player_sprint.to_walk(player)
  playerphysics.remove_physics_factor(player, "speed", "player_sprint")
end


function player_sprint.on_step(dtime)
  for _, player in pairs(minetest.get_connected_players()) do
    local name = player:get_player_name()
    local ctrl = player_info[name]
    if not ctrl then return end
    if ctrl.just_pressed.aux1 and ctrl.can_sprint then
      playerphysics.add_physics_factor(player, "speed", "player_sprint", 2)
    elseif ctrl.just_released.aux1 then
      player_sprint.to_walk(player)
    end
  end
end

minetest.register_on_dieplayer(player_sprint.to_walk)
minetest.register_on_joinplayer(player_sprint.to_walk)

minetest.register_globalstep(player_sprint.on_step)
