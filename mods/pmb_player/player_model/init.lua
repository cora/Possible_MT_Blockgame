
minetest.register_on_joinplayer(function(player)
  -- player:set_eye_offset(vector.new(0, 0, 0), vector.new(4, 5, 0)) -- enable for slightly more usable 3rd person
  player:set_properties({
    mesh = 'humanoid.b3d',
    textures = {'humanoid.png'},
    visual = "mesh",
    visual_size = {x=1, y=1},
    damage_texture_modifier = "^[colorize:red:130",
  })
  player:set_animation({x=0, y=60}, 24)
end)

player_model = {}

function player_model.on_step(dtime)
  for _, player in pairs(minetest.get_connected_players()) do
    local name = player:get_player_name()
    if not player_info[name] then return end
    local c = player_info[name].ctrl
    if c.up or c.down or c.left or c.right then
      player:set_animation({x=70, y=90}, 24, 0.2)
    else
      player:set_animation({x=0, y=60}, 24, 0.2)
    end
  end
end

minetest.register_globalstep(player_model.on_step)