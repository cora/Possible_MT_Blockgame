

local nodes_to_check = {
  feet = vector.new(0,   0,    0),
  head = vector.new(0,   1.5,  0),
  below = vector.new(0, -0.05, 0),
  above = vector.new(0,  2.05, 0),
}

function player_info.positions_on_step(player, dtime)
  local name = player:get_player_name()
  local pos = player:get_pos()
  for node, vect in pairs(nodes_to_check) do
    player_info[name].nodes[node] = minetest.get_node(vector.add(pos, vect)).name
  end

  if minetest.get_item_group(player_info[name].nodes["feet"], "liquid") ~= 0 then
    player_info[name].in_liquid = true
  else
    player_info[name].in_liquid = false
  end
end