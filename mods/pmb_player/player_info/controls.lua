

function player_info.release(player, key)
  player_info[player].ctrl[key] = false
  player_info[player].just_released[key] = true
end
function player_info.press(player, key)
  player_info[player].ctrl[key] = true
  player_info[player].just_pressed[key] = true
end
-- function player_info.hold(player, key, dtime)
--   player_info[player].since_pressed[key] = player_info[player].since_pressed[key] + dtime
-- end
function player_info.reset_just_pressed(player)
  player_info[player].just_pressed = {}
  player_info[player].just_released = {}
end
function player_info.count_time_since_released(player, dtime)
  for key, is_key in pairs(player_info[player].ctrl) do


    if player_info[player].just_pressed[key] then
      player_info[player].since_pressed[key] = dtime
    end
    if player_info[player].just_released[key] then
      player_info[player].since_released[key] = dtime
    end

    player_info[player].since_pressed[key] = player_info[player].since_pressed[key] + dtime
    player_info[player].since_released[key] = player_info[player].since_released[key] + dtime
  end
end

function player_info.controls_on_step(player, dtime)
  local name = player:get_player_name()
  local ctrl = player:get_player_control()

  -- keep track of how long it's been since you pressed a key
  -- do this now so it's a step behind the ctrl update
  player_info.count_time_since_released(name, dtime)

  player_info.reset_just_pressed(name)

  -- go through each key and compare them to the player's keys
  if ctrl then
    for key, oldval in pairs(player_info[name].ctrl) do

      -- if it's changes, say so
      if oldval ~= ctrl[key] then
        if oldval == false then
          player_info.press(name, key)
        elseif oldval == true then
          player_info.release(name, key)
        end
      -- if it's the same, count how long it's been the same
      elseif oldval == ctrl[key]
      and ctrl[key] then
        -- player_info.hold(name, key, dtime)
      end
    end
  end
end