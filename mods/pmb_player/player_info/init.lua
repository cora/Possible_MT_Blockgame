
local mod_name = minetest.get_current_modname()
local mod_path = minetest.get_modpath(mod_name)


player_info = {}


function player_info.player_die()
end
function player_info.player_leave()
end
function player_info.player_join()
end

local function get_controls_blank(val)
  return {
    sneak = val,
    up    = val,
    down  = val,
    left  = val,
    right = val,
    jump  = val,
    aux1  = val,
    aux2  = val,
    dig   = val,
    place = val,
  }
end

local function get_shell()
  return {
    ctrl = get_controls_blank(false),
    just_released = {},
    just_pressed = {},
    since_pressed = get_controls_blank(0),
    since_released = get_controls_blank(0),
    health = 20,
    hunger = 20,
    can_sprint = true,
    creative = false,
    nodes = {
      feet = "",
      head = "",
      below = "",
      above = "",},
    on_floor = false,
    in_liquid = false,
    height = 1.7,
  }
end


dofile(mod_path .. DIR_DELIM .. "controls.lua")
dofile(mod_path .. DIR_DELIM .. "positions.lua")
dofile(mod_path .. DIR_DELIM .. "debug.lua")


function player_info.on_step(dtime)
  for _, player in pairs(minetest.get_connected_players()) do
    local name = player:get_player_name()
    if not player_info[name] then player_info[name] = get_shell() end
    player_info.controls_on_step(player, dtime)
    player_info.positions_on_step(player, dtime)
  end
end

minetest.register_globalstep(player_info.on_step)

minetest.register_on_joinplayer(player_info.player_join)
minetest.register_on_leaveplayer(player_info.player_leave)
minetest.register_on_dieplayer(player_info.player_die)

