player_swimming = {}

function player_swimming.on_step(dtime)
  for _, player in pairs(minetest.get_connected_players()) do
    local name = player:get_player_name()
    if player_info[name].in_liquid then
      if minetest.get_item_group(player_info[name].nodes.feet, "lava") == 0 then
        player:add_velocity(vector.new(0, 0, 0))
      end
    end
  end
end


-- enable later when water is properly implemented
-- minetest.register_globalstep(player_swimming.on_step)
