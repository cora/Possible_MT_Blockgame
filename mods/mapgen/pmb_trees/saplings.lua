

pmb_trees.grow_tree = {}

pmb_trees.schematics = {}

function pmb_trees.register_sapling(name)
  local node_name = string.lower(name)
  pmb_trees.grow_tree[node_name] = pmb_trees.grow_tree[node_name] or function() return nil end

  minetest.register_node(("pmb_trees:" .. node_name .. "_sapling"), {
    description = name .. " sapling",
    drawtype = "plantlike",
    waving = 1,
    visual_scale = 1.0,
    tiles = {"pmb_sapling_" .. node_name .. ".png"},
    inventory_image = "pmb_sapling_" .. node_name .. ".png",
    wield_image = "pmb_sapling_" .. node_name .. ".png",
    paramtype = "light",
    sunlight_propagates = true,
    walkable = false,
    buildable_to = true,
    groups = { ["item_"..node_name.."_sapling"] = 1, dig_immediate = 3, sapling = 1,},
    -- drop = {"pmb_trees:" .. node_name .. "_sapling"},
    -- sounds = {},
    selection_box = {
      type = "fixed",
      fixed = {-5/16, -8/16, -5/16, 5/16, 1/16, 5/16},
    },
    is_ground_content = true,
    _tree_type = node_name,
    _tree_grow = function(pos, node)
      if type(pmb_trees.grow_tree[name]) == "function" then
        pmb_trees.grow_tree[name](pos, node)
      else
        pmb_trees.grow_tree.general(pos, node, node_name)
      end
    end,
    on_place = function(itemstack, placer, pointed_thing)
      return pmb_util.only_place_above(itemstack, placer, pointed_thing, {"soil"})
    end,
    -- on_rightclick = function (pos, node)
    --   local nn = minetest.registered_nodes[node.name]._tree_type
    --   pmb_trees.grow_tree.general(pos, node, nn)
    -- end,
  })
end

pmb_trees.register_sapling("Oak")
pmb_trees.register_sapling("Ash")
pmb_trees.register_sapling("Spruce")


-- schematics and growing trees
local sch = pmb_trees.get_schematic_path

function pmb_trees.register_tree_schematic(kind, name)
  pmb_trees.schematics[kind] = pmb_trees.schematics[kind] or {}
  pmb_trees.schematics[kind][#pmb_trees.schematics[kind]+1] = sch(name)
end

-- OAK
pmb_trees.register_tree_schematic("oak", "oak_0")
pmb_trees.register_tree_schematic("oak", "oak_1")
pmb_trees.register_tree_schematic("oak", "oak_2")
pmb_trees.register_tree_schematic("oak", "oak_huge_0")

-- ASH
pmb_trees.register_tree_schematic("ash", "ash_0")
pmb_trees.register_tree_schematic("ash", "ash_1")
pmb_trees.register_tree_schematic("ash", "ash_2")

-- SPRUCE
pmb_trees.register_tree_schematic("spruce", "spruce_1")
pmb_trees.register_tree_schematic("spruce", "spruce_2")
pmb_trees.register_tree_schematic("spruce", "spruce_3")



function pmb_trees.grow_tree.general(pos, node, name)
  local variant = math.random(1, #pmb_trees.schematics[name])
  minetest.log(variant)
  variant = pmb_trees.schematics[name][variant]
  minetest.set_node(pos, {name = "air"})
  minetest.place_schematic(pos, variant, "random", nil, false, {place_center_x=true, place_center_z=true})
end
