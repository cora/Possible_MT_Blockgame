


minetest.register_abm({
  nodenames = {"group:leaf_decay"},
  neighbors = {'air'},
  interval = 5.0,
  chance = 20,
  action = function(pos, node, active_object_count, active_object_count_wider)
		local n = minetest.find_node_near(pos, 3, {"group:wood_log"})
    if n then return false end
    minetest.dig_node(pos)
  end
})

local function is_air_above(pos, limit)
  local v = vector.copy(pos)
  for i=1, limit do
    v = vector.offset(v, 0, 1, 0)
    local node = minetest.get_node(v)
    if node and minetest.registered_nodes[node.name]
    and minetest.registered_nodes[node.name].walkable then
      return false
    end
  end
  return true
end

minetest.register_abm({
  nodenames = {"group:sapling"},
  interval = 20.0,
  chance = 50,
  action = function(pos, node, active_object_count, active_object_count_wider)
    if is_air_above(pos, 9) then
      local tree_type = minetest.registered_nodes[node.name]._tree_type
      pmb_trees.grow_tree.general(pos, node, tree_type)
    end
  end
})


