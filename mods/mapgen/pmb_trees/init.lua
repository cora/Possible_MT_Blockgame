local mod_name = minetest.get_current_modname()
local mod_path = minetest.get_modpath(mod_name)

pmb_trees = {}

function pmb_trees.get_schematic_path(name)
  return (mod_path .. DIR_DELIM .. "schematics" .. DIR_DELIM .. "pmb_tree_" .. name .. ".mts")
end


dofile(mod_path .. DIR_DELIM .. "saplings.lua")
dofile(mod_path .. DIR_DELIM .. "abms.lua")