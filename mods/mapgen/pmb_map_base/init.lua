
local mod_name = minetest.get_current_modname()
local mod_path = minetest.get_modpath(mod_name)

pmb_map_base = {}

dofile(mod_path .. DIR_DELIM .. "aliases.lua")
dofile(mod_path .. DIR_DELIM .. "biomes.lua")
dofile(mod_path .. DIR_DELIM .. "ores.lua")
