local mod_name = minetest.get_current_modname()
local mod_path = minetest.get_modpath(mod_name)


pmb_biomes = {}
pmb_biomes.registered = {}

function pmb_biomes.register_biome(biome, groups)
  -- pmb_biomes[biome.name] = groups
  pmb_biomes.registered[biome.name] = 1
  minetest.register_biome(biome)
  for _, group in pairs(groups) do
    if not pmb_biomes[group] then
      pmb_biomes[group] = {biome.name}
    else
      pmb_biomes[group][#pmb_biomes[group]+1] = biome.name
    end
  end
end





dofile(mod_path .. DIR_DELIM .. "biomes" .. DIR_DELIM .. "grasslands.lua")
dofile(mod_path .. DIR_DELIM .. "biomes" .. DIR_DELIM .. "ash_forest.lua")
dofile(mod_path .. DIR_DELIM .. "biomes" .. DIR_DELIM .. "ash_forest_dense.lua")
dofile(mod_path .. DIR_DELIM .. "biomes" .. DIR_DELIM .. "oak_forest.lua")
dofile(mod_path .. DIR_DELIM .. "biomes" .. DIR_DELIM .. "oak_forest_dense.lua")
dofile(mod_path .. DIR_DELIM .. "biomes" .. DIR_DELIM .. "oak_forest_dense_1.lua")
dofile(mod_path .. DIR_DELIM .. "biomes" .. DIR_DELIM .. "ocean.lua")
dofile(mod_path .. DIR_DELIM .. "biomes" .. DIR_DELIM .. "ruined_valley.lua")
dofile(mod_path .. DIR_DELIM .. "biomes" .. DIR_DELIM .. "tundra.lua")
dofile(mod_path .. DIR_DELIM .. "biomes" .. DIR_DELIM .. "boulder_valley.lua")
-- dofile(mod_path .. DIR_DELIM .. "biomes" .. DIR_DELIM .. "beach_bridges.lua")


