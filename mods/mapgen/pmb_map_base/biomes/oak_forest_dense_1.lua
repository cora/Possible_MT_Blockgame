
local sealevel = pmb_vars.overworld.sea_level
local alt_max = pmb_vars.overworld.alt_max
local alt_min = pmb_vars.overworld.alt_min

-------------------------------
------------ BIOME ------------
-------------------------------

pmb_biomes.register_biome({
  name = 'oak_forest_dense_1',

  node_top = 'pmb_soil:forest_dirt',
  depth_top = 1,

  node_filler = 'pmb_soil:dirt',
  depth_filler = 5,

  node_riverbed = 'pmb_soil:sand',
  depth_riverbed = 3,

  -- TODO: more calculated mapgen, less magic numbers
  y_max = alt_max,
  y_min = sealevel,
  vertical_blend = 1,

  heat_point = 57,
  humidity_point = 63,
}, {"forest", "oak", "overworld"})

-------------------------------
--------- DECORATIONS ---------
-------------------------------

local sch = pmb_trees.get_schematic_path


-- lots of bunched up trees
minetest.register_decoration({
  deco_type = "schematic",
  place_on = {"group:soil"},
  sidelen = 80,
  noise_params = {
    offset = -0.0,
    scale = 0.03,
    spread = {x = 1, y = 150, z = 1},
    seed = 654,
    octaves = 4,
    persist = 0.1
  },
  biomes = {"oak_forest_dense_1"},
  y_max = alt_max,
  y_min = sealevel,
  schematic = sch("oak_2"),
  flags = "place_center_x, place_center_z",
  rotation = "random",
})
-- GIANT oaks
minetest.register_decoration({
  deco_type = "schematic",
  place_on = {"group:soil"},
  sidelen = 80,
  noise_params = {
    offset = -0.0,
    scale = 0.03,
    spread = {x = 1, y = 150, z = 1},
    seed = 8976,
    octaves = 4,
    persist = 0.1
  },
  biomes = {"oak_forest_dense_1"},
  y_max = alt_max,
  y_min = sealevel,
  schematic = sch("oak_huge_0"),
  flags = "place_center_x, place_center_z",
  rotation = "random",
})
-- GIANT oaks
minetest.register_decoration({
  deco_type = "schematic",
  place_on = {"group:soil"},
  sidelen = 80,
  fill_ratio = 0.002,
  biomes = {"oak_forest_dense"},
  y_max = alt_max,
  y_min = sealevel,
  schematic = sch("oak_huge_2"),
  flags = "place_center_x, place_center_z",
  rotation = "random",
  place_offset_y = -3
})
-- really huge tree
minetest.register_decoration({
  deco_type = "schematic",
  place_on = {"group:soil"},
  sidelen = 80,
  fill_ratio = 0.002,
  biomes = {"oak_forest_dense"},
  y_max = alt_max,
  y_min = sealevel,
  schematic = sch("oak_huge_1"),
  flags = "place_center_x, place_center_z, force_placement",
  rotation = "random",
  place_offset_y = -0
})


-- bushes in large sparse clumps
minetest.register_decoration({
  deco_type = "schematic",
  place_on = {"group:soil"},
  sidelen = 80,
  noise_params = {
    offset = -0.0,
    scale = 0.03,
    spread = {x = 1, y = 150, z = 1},
    seed = 867,
    octaves = 4,
    persist = 0.1
  },
  biomes = {"oak_forest_dense_1"},
  y_max = alt_max,
  y_min = sealevel,
  schematic = sch("oak_bush_0"),
  flags = "place_center_x, place_center_z",
  rotation = "random",
})



-- FOREST GRASS
minetest.register_ore({
  ore_type	= "blob",
  ore		    = "pmb_soil:dirt_with_grass",
  wherein		= {"pmb_soil:forest_dirt"},
  clust_scarcity	= 150,
  clust_num_ores	= 5,
  clust_size	= 7,
  y_min		= -100,
  y_max		= 3000,
  noise_params = {
    offset  = 0.005,
    scale   = 1,
    spread  = {x=250, y=250, z=250},
    seed    = 12345,
    octaves = 3,
    persist = 0.6,
    lacunarity = 2,
    flags = "defaults",
  },
  biomes = { "oak_forest_dense_1" },
})

minetest.register_ore({
  ore_type	= "scatter",
  ore		    = "pmb_soil:forest_grass",
  wherein		= {"pmb_soil:forest_dirt"},
  clust_scarcity	= 250,
  clust_num_ores	= 40,
  clust_size	= 7,
  y_min		= -100,
  y_max		= 3000,
  biomes = { "oak_forest_dense_1" },
})
minetest.register_ore({
  ore_type	= "scatter",
  ore		    = "pmb_stone:stone",
  wherein		= {"group:soil"},
  clust_scarcity	= 1050,
  clust_num_ores	= 20,
  clust_size	= 3,
  y_min		= -3,
  y_max		= 3000,
  biomes = { "oak_forest_dense_1" },
})


minetest.register_decoration({
  deco_type = "schematic",
  place_on = {"group:soil"},
  sidelen = 80,
  noise_params = {
    offset = 0,
    scale = 0.0015,
    spread = {x = 50, y = 200, z = 50},
    seed = 354,
    octaves = 2,
    persistence = 0.7,
    lacunarity = 2.0,
  },
  biomes = {"oak_forest_dense_1"},
  y_max = alt_max,
  y_min = sealevel,
  schematic = sch("ash_1"),
  flags = "place_center_x, place_center_z",
  rotation = "random",
})
