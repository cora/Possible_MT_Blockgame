
local sealevel = pmb_vars.overworld.sea_level
local alt_max = pmb_vars.overworld.alt_max
local alt_min = pmb_vars.overworld.alt_min

-------------------------------
------------ BIOME ------------
-------------------------------

pmb_biomes.register_biome({
  name = 'grasslands',

  node_top = 'pmb_soil:dirt_with_grass',
  depth_top = 1,

  node_filler = 'pmb_soil:dirt',
  depth_filler = 5,

  node_riverbed = 'pmb_soil:sand',
  depth_riverbed = 3,

  -- TODO: more calculated mapgen, less magic numbers
  y_max = alt_max,
  y_min = sealevel,
  vertical_blend = 1,

  heat_point = 50,
  humidity_point = 50,
}, {"field", "overworld", "clearing"})

-------------------------------
--------- DECORATIONS ---------
-------------------------------

local sch = pmb_trees.get_schematic_path

-- minetest.place_schematic(pos, schematic_path, "random", nil, false)

-- sparse trees
minetest.register_decoration({
  deco_type = "schematic",
  place_on = {"group:soil"},
  sidelen = 80,
  noise_params = {
    offset = 0.00055,
    scale = 0.001,
    spread = {x = 250, y = 250, z = 250},
    seed = 3 + 5,
    octaves = 3,
    persist = 0.66
  },
  biomes = {"grasslands"},
  y_max = alt_max,
  y_min = sealevel,
  schematic = sch("oak_0"),
  flags = "place_center_x, place_center_z",
  rotation = "random",
})

-- sparse trees
minetest.register_decoration({
  deco_type = "schematic",
  place_on = {"group:soil"},
  sidelen = 80,
  noise_params = {
    offset = -0.002,
    scale = 0.002,
    spread = {x = 200, y = 200, z = 200},
    seed = 364,
    octaves = 2,
    persist = 0.6
  },
  biomes = {"grasslands"},
  y_max = alt_max,
  y_min = sealevel,
  schematic = sch("oak_2"),
  flags = "place_center_x, place_center_z",
  rotation = "random",
})
-- bushes in large sparse clumps
minetest.register_decoration({
  deco_type = "schematic",
  place_on = {"group:soil"},
  sidelen = 80,
  noise_params = {
    offset = -0.001,
    scale = 0.01,
    spread = {x = 250, y = 250, z = 250},
    seed = 39,
    octaves = 3,
    persist = 0.6
  },
  biomes = {"grasslands"},
  y_max = alt_max,
  y_min = sealevel,
  schematic = sch("oak_bush_0"),
  flags = "place_center_x, place_center_z",
  rotation = "random",
})
-- bushes in large sparse clumps
minetest.register_decoration({
  deco_type = "schematic",
  place_on = {"group:soil"},
  sidelen = 80,
  noise_params = {
    offset = -0.001,
    scale = 0.02,
    spread = {x = 2, y = 20, z = 2},
    seed = 367,
    octaves = 3,
    persist = 0.6
  },
  biomes = {"grasslands"},
  y_max = alt_max,
  y_min = sealevel,
  schematic = sch("oak_huge_0"),
  flags = "place_center_x, place_center_z",
  rotation = "random",
})