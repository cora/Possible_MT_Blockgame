
local sealevel = pmb_vars.overworld.sea_level
local alt_max = pmb_vars.overworld.alt_max
local alt_min = pmb_vars.overworld.alt_min

-------------------------------
------------ BIOME ------------
-------------------------------

pmb_biomes.register_biome({
  name = 'ash_forest',

  node_top = 'pmb_soil:dirt_with_grass',
  depth_top = 1,

  node_filler = 'pmb_soil:dirt',
  depth_filler = 5,

  node_riverbed = 'pmb_soil:sand',
  depth_riverbed = 3,

  -- TODO: more calculated mapgen, less magic numbers
  y_max = alt_max,
  y_min = sealevel,
  vertical_blend = 1,

  heat_point = 30,
  humidity_point = 45,
}, {"forest", "ash", "overworld"})

-------------------------------
--------- DECORATIONS ---------
-------------------------------

local sch = pmb_trees.get_schematic_path



-- lots of bunched up trees
minetest.register_decoration({
  deco_type = "schematic",
  place_on = {"group:soil"},
  sidelen = 80,
  noise_params = {
    offset = -0,
    scale = 0.0095,
    spread = {x = 5, y = 200, z = 5},
    seed = 354,
    octaves = 3,
    persistence = 0.7,
    lacunarity = 2.0,
  },
  biomes = {"ash_forest"},
  y_max = alt_max,
  y_min = sealevel,
  schematic = sch("oak_bush_0"),
  flags = "place_center_x, place_center_z",
  rotation = "random",
})

-- GIANT ash
minetest.register_decoration({
  deco_type = "schematic",
  place_on = {"group:soil"},
  sidelen = 80,
  noise_params = {
    offset = -0.001,
    scale = 0.0185,
    spread = {x = 100, y = 100, z = 100},
    seed = 64758,
    octaves = 3,
    persistence = 0.2,
    lacunarity = 2.0,
  },
  biomes = {"ash_forest"},
  y_max = alt_max,
  y_min = sealevel,
  schematic = sch("ash_0"),
  flags = "place_center_x, place_center_z",
  rotation = "random",
})


-- ash 2
minetest.register_decoration({
  deco_type = "schematic",
  place_on = {"group:soil"},
  sidelen = 80,
  -- fill_ratio = 0.001,
  noise_params = {
    offset = -0.004,
    scale = 0.0075,
    spread = {x = 10, y = 1000, z = 10},
    seed = 71,
    octaves = 2,
    persistence = 0.2,
    lacunarity = 1.0,
  },
  biomes = {"ash_forest"},
  y_max = alt_max,
  y_min = sealevel,
  schematic = sch("ash_2"),
  flags = "place_center_x, place_center_z",
  rotation = "random",
})

-- ash
minetest.register_decoration({
  deco_type = "schematic",
  place_on = {"group:soil"},
  sidelen = 80,
  fill_ratio = 0.0016,
  -- noise_params = {
  --   offset = -0.004,
  --   scale = 0.0175,
  --   spread = {x = 10, y = 10, z = 10},
  --   seed = 6,
  --   octaves = 8,
  --   persistence = 0.7,
  --   lacunarity = 2.0,
  -- },
  biomes = {"ash_forest"},
  y_max = alt_max,
  y_min = sealevel,
  schematic = sch("ash_1"),
  flags = "place_center_x, place_center_z",
  rotation = "random",
})