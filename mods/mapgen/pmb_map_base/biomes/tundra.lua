
local sealevel = pmb_vars.overworld.sea_level
local alt_max = pmb_vars.overworld.alt_max
local alt_min = pmb_vars.overworld.alt_min

-------------------------------
------------ BIOME ------------
-------------------------------

pmb_biomes.register_biome({
  name = 'tundra',

  node_top = 'pmb_soil:grass_snow',
  depth_top = 1,

  node_filler = 'pmb_soil:dirt',
  depth_filler = 5,

  node_riverbed = 'pmb_soil:sand',
  depth_riverbed = 3,

  -- node_water_top = "pmb_soil:snow",
  -- depth_water_top = 1,


  -- TODO: more calculated mapgen, less magic numbers
  y_max = alt_max,
  y_min = sealevel - 1,
  vertical_blend = 1,

  heat_point = 30,
  humidity_point = 40,
}, {"cold", "ash", "overworld"})

-------------------------------
--------- DECORATIONS ---------
-------------------------------

local sch = pmb_trees.get_schematic_path



-- lots of bunched up trees
minetest.register_decoration({
  deco_type = "schematic",
  place_on = {"group:soil"},
  sidelen = 80,
  noise_params = {
    offset = 0,
    scale = 0.0095,
    spread = {x = 500, y = 200, z = 500},
    seed = 354,
    octaves = 3,
    persistence = 0.7,
    lacunarity = 2.0,
  },
  biomes = {"tundra"},
  y_max = alt_max,
  y_min = sealevel,
  schematic = sch("spruce_3"),
  flags = "place_center_x, place_center_z",
  rotation = "random",
})

-- GIANT ash
minetest.register_decoration({
  deco_type = "schematic",
  place_on = {"group:soil"},
  sidelen = 80,
  noise_params = {
    offset = -0.001,
    scale = 0.0055,
    spread = {x = 100, y = 100, z = 100},
    seed = 28,
    octaves = 4,
    persistence = 0.7,
    lacunarity = 2.0,
  },
  biomes = {"tundra"},
  y_max = alt_max,
  y_min = sealevel,
  schematic = sch("spruce_1"),
  flags = "place_center_x, place_center_z",
  rotation = "random",
})


-- ash
minetest.register_decoration({
  deco_type = "schematic",
  place_on = {"group:soil"},
  sidelen = 80,
  noise_params = {
    offset = -0.004,
    scale = 0.0075,
    spread = {x = 100, y = 100, z = 100},
    seed = 71,
    octaves = 4,
    persistence = 0.7,
    lacunarity = 2.0,
  },
  biomes = {"tundra"},
  y_max = alt_max,
  y_min = sealevel,
  schematic = sch("spruce_2"),
  flags = "place_center_x, place_center_z",
  rotation = "random",
})


-- bush
minetest.register_decoration({
  deco_type = "schematic",
  place_on = {"group:soil"},
  sidelen = 80,
  noise_params = {
    offset = -0.0004,
    scale = 0.0075,
    spread = {x = 100, y = 100, z = 100},
    seed = 4,
    octaves = 4,
    persistence = 0.2,
    lacunarity = 1.0,
  },
  biomes = {"tundra"},
  y_max = alt_max,
  y_min = sealevel,
  schematic = sch("oak_bush_0"),
  flags = "place_center_x, place_center_z",
  rotation = "random",
})

-- bush
minetest.register_decoration({
  deco_type = "schematic",
  place_on = {"group:soil"},
  sidelen = 80,
  noise_params = {
    offset = -0.0004,
    scale = 0.0075,
    spread = {x = 100, y = 100, z = 100},
    seed = 98067,
    octaves = 4,
    persistence = 0.2,
    lacunarity = 1.0,
  },
  biomes = {"tundra"},
  y_max = alt_max,
  y_min = sealevel,
  schematic = sch("spruce_0"),
  flags = "place_center_x, place_center_z",
  rotation = "random",
})


