
local sealevel = pmb_vars.overworld.sea_level
local alt_max = pmb_vars.overworld.alt_max
local alt_min = pmb_vars.overworld.alt_min

-------------------------------
------------ BIOME ------------
-------------------------------

pmb_biomes.register_biome({
  name = 'ocean',

  node_top = 'pmb_soil:sand',
  depth_top = 1,

  node_filler = 'pmb_soil:sand',
  depth_filler = 2,

  node_riverbed = 'pmb_soil:sand',
  depth_riverbed = 3,

  y_max = sealevel + 1,
  y_min = sealevel - 100,

  heat_point = 50,
  humidity_point = 70,
}, {"ocean", "water", "overworld"})



-------------------------------
--------- DECORATIONS ---------
-------------------------------
