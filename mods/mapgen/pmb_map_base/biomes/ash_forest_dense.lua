
local sealevel = pmb_vars.overworld.sea_level
local alt_max = pmb_vars.overworld.alt_max
local alt_min = pmb_vars.overworld.alt_min

-------------------------------
------------ BIOME ------------
-------------------------------

pmb_biomes.register_biome({
  name = 'ash_forest_dense',

  node_top = 'pmb_soil:forest_dirt',
  depth_top = 1,

  node_filler = 'pmb_soil:dirt',
  depth_filler = 5,

  node_riverbed = 'pmb_soil:sand',
  depth_riverbed = 3,

  -- TODO: more calculated mapgen, less magic numbers
  y_max = alt_max,
  y_min = sealevel,
  vertical_blend = 1,

  heat_point = 35,
  humidity_point = 50,
}, {"forest", "ash", "overworld"})

-------------------------------
--------- DECORATIONS ---------
-------------------------------

local sch = pmb_trees.get_schematic_path



-- FOREST GRASS
minetest.register_ore({
  ore_type	= "blob",
  ore		    = "pmb_soil:forest_grass",
  wherein		= {"pmb_soil:forest_dirt"},
  clust_scarcity	= 50,
  clust_num_ores	= 5,
  clust_size	= 9,
  y_min		= sealevel,
  y_max		= alt_max,
  noise_params = {
    offset  = 0.01,
    scale   = 1,
    spread  = {x=250, y=250, z=250},
    seed    = 12345,
    octaves = 3,
    persist = 0.6,
    lacunarity = 2,
    flags = "defaults",
  },
  biomes = { "ash_forest_dense" },
})

minetest.register_ore({
  ore_type	= "scatter",
  ore		    = "pmb_soil:forest_grass",
  wherein		= {"pmb_soil:dirt_with_grass"},
  clust_scarcity	= 350,
  clust_num_ores	= 40,
  clust_size	= 7,
  y_min		= sealevel,
  y_max		= alt_max,
  noise_params = {
    offset  = 0.01,
    scale   = 1,
    spread  = {x=250, y=250, z=250},
    seed    = 12345,
    octaves = 3,
    persist = 0.6,
    lacunarity = 2,
    flags = "defaults",
  },
  biomes = { "ash_forest_dense" },
})



-- lots of bunched up trees
minetest.register_decoration({
  deco_type = "schematic",
  place_on = {"pmb_soil:forest_grass"},
  sidelen = 80,
  noise_params = {
    offset = 0,
    scale = 0.05,
    spread = {x = 150, y = 150, z = 150},
    seed = 96,
    octaves = 2,
    persist = 0.1
  },
  biomes = {"ash_forest_dense"},
  y_min		= sealevel,
  y_max		= alt_max,
  schematic = sch("ash_2"),
  flags = "place_center_x, place_center_z",
  rotation = "random",
})
-- GIANT ashs
minetest.register_decoration({
  deco_type = "schematic",
  place_on = {"pmb_soil:forest_grass"},
  sidelen = 80,
  noise_params = {
    offset = 0.02,
    scale = 0.02,
    spread = {x = 150, y = 150, z = 150},
    seed = 896,
    octaves = 2,
    persist = 0.1
  },
  biomes = {"ash_forest_dense"},
  y_min		= sealevel,
  y_max		= alt_max,
  schematic = sch("ash_0"),
  flags = "place_center_x, place_center_z",
  rotation = "random",
})
-- lots of bunched up trees
minetest.register_decoration({
  deco_type = "schematic",
  place_on = {"pmb_soil:forest_grass"},
  sidelen = 80,
  noise_params = {
    offset = 0,
    scale = 0.03,
    spread = {x = 150, y = 150, z = 150},
    seed = 7,
    octaves = 2,
    persist = 0.1
  },
  biomes = {"ash_forest_dense"},
  y_min		= sealevel,
  y_max		= alt_max,
  schematic = sch("ash_3"),
  flags = "place_center_x, place_center_z",
  rotation = "random",
})
-- lots of bunched up trees
minetest.register_decoration({
  deco_type = "schematic",
  place_on = {"pmb_soil:forest_grass"},
  sidelen = 80,
  noise_params = {
    offset = 0,
    scale = 0.03,
    spread = {x = 150, y = 150, z = 150},
    seed = 7,
    octaves = 2,
    persist = 0.1
  },
  biomes = {"ash_forest_dense"},
  y_min		= sealevel,
  y_max		= alt_max,
  schematic = sch("ash_3"),
  flags = "place_center_x, place_center_z",
  rotation = "random",
  place_offset_y = -3
})


-- bushes in large sparse clumps
minetest.register_decoration({
  deco_type = "schematic",
  place_on = {"pmb_soil:forest_grass"},
  sidelen = 80,
  noise_params = {
    offset = -0.01,
    scale = 0.02,
    spread = {x = 250, y = 250, z = 250},
    seed = 3,
    octaves = 2,
    persist = 0.6
  },
  biomes = {"ash_forest_dense"},
  y_min		= sealevel,
  y_max		= alt_max,
  schematic = sch("oak_bush_0"),
  flags = "place_center_x, place_center_z",
  rotation = "random",
})





-- lots of bunched up trees
minetest.register_decoration({
  deco_type = "schematic",
  place_on = {"pmb_soil:forest_grass"},
  sidelen = 80,
  noise_params = {
    offset = 0,
    scale = 0.0015,
    spread = {x = 500, y = 200, z = 500},
    seed = 354,
    octaves = 2,
    persistence = 0.7,
    lacunarity = 2.0,
  },
  biomes = {"ash_forest_dense"},
  y_min		= sealevel,
  y_max		= alt_max,
  schematic = sch("ash_1"),
  flags = "place_center_x, place_center_z",
  rotation = "random",
})


minetest.register_ore({
  ore_type	= "scatter",
  ore		    = "pmb_soil:forest_dirt",
  wherein		= {"pmb_soil:forest_grass"},
  clust_scarcity	= 50,
  clust_num_ores	= 40,
  clust_size	= 7,
  y_min		= sealevel,
  y_max		= alt_max,
  noise_params = {
    offset  = 0.01,
    scale   = 1,
    spread  = {x=250, y=250, z=250},
    seed    = 12345,
    octaves = 3,
    persist = 0.6,
    lacunarity = 2,
    flags = "defaults",
  },
  biomes = { "ash_forest_dense" },
})

