
local sealevel = pmb_vars.overworld.sea_level
local alt_max = pmb_vars.overworld.alt_max
local alt_min = pmb_vars.overworld.alt_min

-------------------------------
------------ BIOME ------------
-------------------------------

pmb_biomes.register_biome({
  name = 'oak_forest',

  node_top = 'pmb_soil:dirt_with_grass',
  depth_top = 1,

  node_filler = 'pmb_soil:dirt',
  depth_filler = 5,

  node_riverbed = 'pmb_soil:sand',
  depth_riverbed = 3,

  -- TODO: more calculated mapgen, less magic numbers
  y_max = alt_max,
  y_min = sealevel,
  vertical_blend = 1,

  heat_point = 45,
  humidity_point = 55,
}, {"forest", "oak", "overworld"})

-------------------------------
--------- DECORATIONS ---------
-------------------------------

local sch = pmb_trees.get_schematic_path


-- lots of bunched up trees
minetest.register_decoration({
  deco_type = "schematic",
  place_on = {"group:soil"},
  sidelen = 80,
  noise_params = {
    offset = -0,
    scale = 0.003,
    spread = {x = 10, y = 10, z = 10},
    seed = 96,
    octaves = 2,
    persist = 0.1
  },
  biomes = {"oak_forest"},
  y_max = alt_max,
  y_min = sealevel,
  schematic = sch("oak_huge_0"),
  flags = "place_center_x, place_center_z",
  rotation = "random",
})

-- lots of bunched up trees
minetest.register_decoration({
  deco_type = "schematic",
  place_on = {"group:soil"},
  sidelen = 80,
  noise_params = {
    offset = -0.01,
    scale = 0.05,
    spread = {x = 50, y = 1000, z = 50},
    seed = 7,
    octaves = 2,
    persist = 0.1
  },
  biomes = {"oak_forest"},
  y_max = alt_max,
  y_min = sealevel,
  schematic = sch("oak_1"),
  flags = "place_center_x, place_center_z",
  rotation = "random",
})


-- lots of bunched up trees
minetest.register_decoration({
  deco_type = "schematic",
  place_on = {"group:soil"},
  sidelen = 80,
  noise_params = {
    offset = -0.0,
    scale = 0.015,
    spread = {x = 20, y = 1000, z = 20},
    seed = 564,
    octaves = 2,
    persist = 0.1
  },
  biomes = {"oak_forest"},
  y_max = alt_max,
  y_min = sealevel,
  schematic = sch("oak_bush_0"),
  flags = "place_center_x, place_center_z",
  rotation = "random",
})

-- ash
minetest.register_decoration({
  deco_type = "schematic",
  place_on = {"group:soil"},
  sidelen = 80,
  noise_params = {
    offset = -0.001,
    scale = 0.0025,
    spread = {x = 1, y = 1, z = 1},
    seed = 28,
    octaves = 8,
    persistence = 0.7,
    lacunarity = 2.0,
  },
  biomes = {"ash_forest"},
  y_max = alt_max,
  y_min = sealevel,
  schematic = sch("ash_1"),
  flags = "place_center_x, place_center_z",
  rotation = "random",
})