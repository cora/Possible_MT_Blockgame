
local sealevel = pmb_vars.overworld.sea_level
local alt_max = pmb_vars.overworld.alt_max
local alt_min = pmb_vars.overworld.alt_min

-------------------------------
------------ BIOME ------------
-------------------------------

pmb_biomes.register_biome({
  name = 'boulder_valley',

  node_top = 'pmb_soil:forest_dirt',--_with_grass',
  depth_top = 1,

  node_filler = 'pmb_soil:dirt',
  depth_filler = 5,

  node_riverbed = 'pmb_soil:sand',
  depth_riverbed = 3,

  -- TODO: more calculated mapgen, less magic numbers
  y_max = sealevel + 30,
  y_min = sealevel,
  vertical_blend = 9,

  heat_point = 65,
  humidity_point = 45,
}, {"light_scrub", "ash", "boulders", "overworld"})

-------------------------------
--------- DECORATIONS ---------
-------------------------------

local sch = pmb_trees.get_schematic_path



-- trees
if true then
  -- oak
  minetest.register_decoration({
    deco_type = "schematic",
    place_on = {"group:soil"},
    sidelen = 80,
    noise_params = {
      offset = 0,
      scale = 0.0065,
      spread = {x = 50, y = 200, z = 50},
      seed = 77,
      octaves = 3,
      persistence = 0.2,
      lacunarity = 1.0,
    },
    biomes = {"boulder_valley"},
    y_max = alt_max,
    y_min = sealevel,
    schematic = sch("oak_huge_0"),
    flags = "place_center_x, place_center_z",
    rotation = "random",
  })
  -- lots of bunched up trees
  minetest.register_decoration({
    deco_type = "schematic",
    place_on = {"group:soil"},
    sidelen = 80,
    noise_params = {
      offset = 0,
      scale = 0.0195,
      spread = {x = 500, y = 200, z = 500},
      seed = 354,
      octaves = 2,
      persistence = 0.2,
      lacunarity = 1.0,
    },
    biomes = {"boulder_valley"},
    y_max = alt_max,
    y_min = sealevel,
    schematic = sch("oak_bush_0"),
    flags = "place_center_x, place_center_z",
    rotation = "random",
  })

  -- GIANT ash
  minetest.register_decoration({
    deco_type = "schematic",
    place_on = {"group:soil"},
    sidelen = 80,
    noise_params = {
      offset = -0.001,
      scale = 0.0185,
      spread = {x = 50, y = 100, z = 50},
      seed = 3224352,
      octaves = 4,
      persistence = 0.2,
      lacunarity = 1.0,
    },
    biomes = {"boulder_valley"},
    y_max = alt_max,
    y_min = sealevel,
    schematic = sch("ash_0"),
    flags = "place_center_x, place_center_z",
    rotation = "random",
  })


  -- ash
  minetest.register_decoration({
    deco_type = "schematic",
    place_on = {"group:soil"},
    sidelen = 80,
    noise_params = {
      offset = -0.004,
      scale = 0.0075,
      spread = {x = 100, y = 100, z = 100},
      seed = 71,
      octaves = 3,
      persistence = 0.7,
      lacunarity = 2.0,
    },
    biomes = {"boulder_valley"},
    y_max = alt_max,
    y_min = sealevel,
    schematic = sch("ash_2"),
    flags = "place_center_x, place_center_z",
    rotation = "random",
  })
end



-- ORES





minetest.register_ore({
  ore_type	= "blob",
  ore		    = "pmb_stone:cobble",
  wherein		= {"group:soil"},
  clust_scarcity	= 350,
  clust_num_ores	= 40,
  clust_size	= 5,
  y_min		= -100,
  y_max		= 3000,
  noise_params = {
    offset  = 0.01,
    scale   = 1,
    spread  = {x=250, y=250, z=250},
    seed    = 12345,
    octaves = 3,
    persist = 0.6,
    lacunarity = 2,
    flags = "defaults",
  },
  biomes = { "boulder_valley" },
})
minetest.register_ore({
  ore_type	= "scatter",
  ore		    = "pmb_stone:cobble",
  wherein		= {"group:soil"},
  clust_scarcity	= 350,
  clust_num_ores	= 40,
  clust_size	= 5,
  y_max = alt_max,
  y_min = sealevel,
  biomes = { "boulder_valley" },
})
minetest.register_ore({
  ore_type	= "blob",
  ore		    = "pmb_stone:stone",
  wherein		= {"group:soil"},
  clust_scarcity	= 350,
  clust_num_ores	= 40,
  clust_size	= 5,
  y_max = alt_max,
  y_min = sealevel,
  noise_params = {
    offset  = 0.01,
    scale   = 1,
    spread  = {x=20, y=250, z=20},
    seed    = 67576,
    octaves = 3,
    persist = 0.6,
    lacunarity = 2,
    flags = "defaults",
  },
  biomes = { "boulder_valley" },
})



minetest.register_ore({
  ore_type	= "blob",
  ore		    = "pmb_stone:cobble_moss_1",
  wherein		= {"pmb_soil:forest_dirt"},
  clust_scarcity	= 350,
  clust_num_ores	= 40,
  clust_size	= 5,
  y_max = alt_max,
  y_min = sealevel,
  noise_params = {
    offset  = 0.01,
    scale   = 1,
    spread  = {x=20, y=250, z=20},
    seed    = 453,
    octaves = 3,
    persist = 0.6,
    lacunarity = 2,
    flags = "defaults",
  },
  biomes = { "boulder_valley" },
})
minetest.register_ore({
  ore_type	= "scatter",
  ore		    = "pmb_stone:cobble_moss_1",
  wherein		= {"pmb_stone:cobble"},
  clust_scarcity	= 250,
  clust_num_ores	= 40,
  clust_size	= 5,
  y_max = alt_max,
  y_min = sealevel,
  biomes = { "boulder_valley" },
})
minetest.register_ore({
  ore_type	= "scatter",
  ore		    = "pmb_stone:cobble_moss_2",
  wherein		= {"pmb_stone:cobble_moss_1"},
  clust_scarcity	= 350,
  clust_num_ores	= 40,
  clust_size	= 5,
  y_max = alt_max,
  y_min = sealevel,
  biomes = { "boulder_valley" },
})