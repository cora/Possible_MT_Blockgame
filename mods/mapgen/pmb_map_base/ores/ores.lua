
local sealevel = pmb_vars.overworld.sea_level
local alt_max = pmb_vars.overworld.alt_max
local alt_min = pmb_vars.overworld.alt_min
local ore_mult = pmb_vars.overworld.ore_multiplier

-- coal
minetest.register_ore({
  ore_type       = "scatter",
  ore            = "pmb_stone:stone_coal_ore",
  wherein        = {"pmb_stone:stone"},
  clust_scarcity = 309050 / ore_mult,
  clust_num_ores = 100,
  clust_size     = 8,
  y_min = alt_min,
  y_max = sealevel - 60,
})
minetest.register_ore({
  ore_type       = "scatter",
  ore            = "pmb_stone:stone_coal_ore", --"pmb_lights:torch", -- use this for testing
  wherein        = {"pmb_stone:stone"},
  clust_scarcity = 109050 / ore_mult,
  clust_num_ores = 20,
  clust_size     = 9,
  y_min = alt_min,
  y_max = sealevel,
})
-- more coal in the mid range to help new players
minetest.register_ore({
  ore_type       = "scatter",
  ore            = "pmb_stone:stone_coal_ore",
  wherein        = {"pmb_stone:stone"},
  clust_scarcity = 650 / ore_mult,
  clust_num_ores = 3,
  clust_size     = 3,
  y_min = sealevel - 10,
  y_max = sealevel + 100,
})

-- IRON
minetest.register_ore({
  ore_type       = "scatter",
  ore            = "pmb_stone:stone_iron_ore",
  wherein        = {"pmb_stone:stone"},
  clust_scarcity = 1050 / ore_mult,
  clust_num_ores = 8,
  clust_size     = 3,
  y_min = alt_min,
  y_max = alt_max,
})
-- lower is more ores?
minetest.register_ore({
  ore_type       = "scatter",
  ore            = "pmb_stone:stone_iron_ore",
  wherein        = {"pmb_stone:stone"},
  clust_scarcity = 2650 / ore_mult,
  clust_num_ores = 12,
  clust_size     = 5,
  y_min = alt_min,
  y_max = sealevel - 20,
})



-- TIN
minetest.register_ore({
  ore_type       = "scatter",
  ore            = "pmb_stone:stone_tin_ore",
  wherein        = {"pmb_stone:stone"},
  clust_scarcity = 1650 / ore_mult,
  clust_num_ores = 8,
  clust_size     = 4,
  y_min = sealevel - 50,
  y_max = alt_max,
})
minetest.register_ore({
  ore_type       = "scatter",
  ore            = "pmb_stone:stone_tin_ore",
  wherein        = {"pmb_stone:stone"},
  clust_scarcity = 2650 / ore_mult,
  clust_num_ores = 4,
  clust_size     = 4,
  y_min = alt_min,
  y_max = alt_max,
})


-- COPPER
minetest.register_ore({
  ore_type       = "scatter",
  ore            = "pmb_stone:stone_copper_ore",
  wherein        = {"pmb_stone:stone"},
  clust_scarcity = 1650 / ore_mult,
  clust_num_ores = 3,
  clust_size     = 4,
  y_min = alt_min,
  y_max = sealevel + 50,
})
minetest.register_ore({
  ore_type       = "scatter",
  ore            = "pmb_stone:stone_copper_ore",
  wherein        = {"pmb_stone:stone"},
  clust_scarcity = 2650 / ore_mult,
  clust_num_ores = 7,
  clust_size     = 4,
  y_min = alt_min,
  y_max = alt_max,
})


-- LAPIS
minetest.register_ore({
  ore_type       = "scatter",
  ore            = "pmb_stone:stone_tin_ore",
  wherein        = {"pmb_stone:stone"},
  clust_scarcity = 1000 / ore_mult,
  clust_num_ores = 5,
  clust_size     = 4,
  y_min = -100,
  y_max = -50,
})
minetest.register_ore({
  ore_type       = "scatter",
  ore            = "pmb_stone:stone_tin_ore",
  wherein        = {"pmb_stone:stone"},
  clust_scarcity = 2500 / ore_mult,
  clust_num_ores = 2,
  clust_size     = 4,
  y_min = -50,
  y_max = 1000,
})



-- DIAMOND
minetest.register_ore({
  ore_type       = "scatter",
  ore            = "pmb_stone:stone_diamond_ore",
  wherein        = {"pmb_stone:stone"},
  clust_scarcity = 2650 / ore_mult,
  clust_num_ores = 4,
  clust_size     = 4,
  y_min = alt_min,
  y_max = sealevel - 50,
})
minetest.register_ore({
  ore_type       = "scatter",
  ore            = "pmb_stone:stone_diamond_ore",
  wherein        = {"pmb_stone:stone"},
  clust_scarcity = 9650 / ore_mult,
  clust_num_ores = 1,
  clust_size     = 1,
  y_min = alt_min,
  y_max = alt_max,
})
minetest.register_ore({
  ore_type       = "scatter",
  ore            = "pmb_stone:stone_diamond_ore",
  wherein        = {"pmb_stone:stone"},
  clust_scarcity = 3650 / ore_mult,
  clust_num_ores = 4,
  clust_size     = 3,
  y_min = math.abs(alt_max - sealevel) * 0.8,
  y_max = alt_max,
})
