
local sealevel = pmb_vars.overworld.sea_level
local alt_max = pmb_vars.overworld.alt_max
local alt_min = pmb_vars.overworld.alt_min


minetest.register_decoration({
  name = "pmb_flora:mushroom_red",
  deco_type = "simple",
  place_on = {"pmb_soil:forest_grass"}, -- only on grasslike nodes
  sidelen = 16,
  noise_params = {
    offset = -0.001,
    scale = 0.06,
    spread = {x = 20, y = 20, z = 20},
    seed = 4654,
    octaves = 3,
    persist = 0.2
  },
  biomes = {},
  y_max = alt_max,
  y_min = sealevel + 1,
  decoration = "pmb_flora:mushroom_red",
})

minetest.register_decoration({
  name = "pmb_flora:mushroom_brown",
  deco_type = "simple",
  place_on = {"pmb_soil:forest_grass"}, -- only on grasslike nodes
  sidelen = 16,
  noise_params = {
    offset = -0.001,
    scale = 0.06,
    spread = {x = 20, y = 20, z = 20},
    seed = 54,
    octaves = 3,
    persist = 0.2
  },
  biomes = {},
  y_max = alt_max,
  y_min = sealevel + 1,
  decoration = "pmb_flora:mushroom_brown",
})