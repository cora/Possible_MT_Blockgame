
local sealevel = pmb_vars.overworld.sea_level
local alt_max = pmb_vars.overworld.alt_max
local alt_min = pmb_vars.overworld.alt_min


minetest.register_ore({
  ore_type	= "blob",
  ore		    = "pmb_soil:gravel",
  wherein		= {"group:stone", "pmb_soil:dirt"},
  clust_scarcity	= 550,
  clust_num_ores	= 10,
  clust_size	= 5,
  y_min		= alt_min,
  y_max		= alt_max,
  biomes = pmb_biomes.registered,
})

minetest.register_ore({
  ore_type	= "blob",
  ore		    = "pmb_stone:granite",
  wherein		= {"group:stone"},
  clust_scarcity	= 50050,
  clust_num_ores	= 40,
  clust_size	= 20,
  y_min		= alt_min,
  y_max		= alt_max,
  biomes = pmb_biomes.registered,
})
