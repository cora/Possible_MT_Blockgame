
local sealevel = pmb_vars.overworld.sea_level
local alt_max = pmb_vars.overworld.alt_max
local alt_min = pmb_vars.overworld.alt_min



local function do_grass_stuff(num, seed)
  minetest.register_decoration({
    name = "pmb_flora:grass_"..num,
    deco_type = "simple",
    place_on = {"pmb_soil:dirt_with_grass"}, -- only on grasslike nodes
    sidelen = 16,
    noise_params = {
      offset = -0.0,
      scale = 0.05,
      spread = {x = 1, y = 2000, z = 2},
      seed = seed,
      octaves = 3,
      persist = 0.2
    },
    biomes = {},
    y_max = alt_max,
    y_min = sealevel + 1,
    decoration = "pmb_flora:grass_"..num,
  })
end


do_grass_stuff(0, 6546)
do_grass_stuff(1, 90)
do_grass_stuff(2, 345)