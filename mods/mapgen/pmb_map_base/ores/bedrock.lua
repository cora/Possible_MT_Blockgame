
local sealevel = pmb_vars.overworld.sea_level
local alt_max = pmb_vars.overworld.alt_max
local alt_min = pmb_vars.overworld.alt_min

local function make_bedrock_layer(height, thickness)
  minetest.register_ore({
    ore_type       = "stratum",
    ore            = "pmb_stone:bedrock",
    wherein        = {"group:solid", "air",},
    y_min = sealevel + height,
    y_max = sealevel + height,
  })
  local min, max
  if thickness > 0 then
    min = sealevel + height
    max = sealevel + height + thickness
  else
    min = sealevel + height + thickness
    max = sealevel + height
  end
  minetest.register_ore({
    ore_type	= "scatter",
    ore		    = "pmb_stone:bedrock",
    wherein   = {"group:solid", "air",},
    y_min		= min,
    y_max		= max,
    clust_scarcity = 9,
    clust_num_ores = 3,
    clust_size     = 3,
  })
  minetest.register_ore({
    ore_type	= "scatter",
    ore		    = "pmb_stone:bedrock",
    wherein   = {"group:solid", "air",},
    y_min		= min,
    y_max		= max,
    clust_scarcity = 9,
    clust_num_ores = 3,
    clust_size     = 2,
  })
end

make_bedrock_layer(pmb_vars.overworld.alt_min, 5)