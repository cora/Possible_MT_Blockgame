local mod_name = minetest.get_current_modname()
local mod_path = minetest.get_modpath(mod_name)

dofile(mod_path .. DIR_DELIM .. "ores" .. DIR_DELIM .. "soil_types.lua")
dofile(mod_path .. DIR_DELIM .. "ores" .. DIR_DELIM .. "ores.lua")
dofile(mod_path .. DIR_DELIM .. "ores" .. DIR_DELIM .. "grasses.lua")
dofile(mod_path .. DIR_DELIM .. "ores" .. DIR_DELIM .. "mushrooms.lua")




dofile(mod_path .. DIR_DELIM .. "ores" .. DIR_DELIM .. "bedrock.lua")

