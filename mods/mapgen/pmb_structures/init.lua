

local mod_name = minetest.get_current_modname()
local mod_path = minetest.get_modpath(mod_name)

pmb_structures = {}

function pmb_structures.get_schematic_path(name)
  return (mod_path .. DIR_DELIM .. "schematics" .. DIR_DELIM .. "pmb_" .. name .. ".mts")
end

local sch = pmb_structures.get_schematic_path


dofile(mod_path .. DIR_DELIM .. "structure_spawning" .. DIR_DELIM .. "cabins.lua")
dofile(mod_path .. DIR_DELIM .. "structure_spawning" .. DIR_DELIM .. "ruins.lua")
dofile(mod_path .. DIR_DELIM .. "structure_spawning" .. DIR_DELIM .. "ruined_bridges.lua")
dofile(mod_path .. DIR_DELIM .. "structure_spawning" .. DIR_DELIM .. "boulders.lua")



