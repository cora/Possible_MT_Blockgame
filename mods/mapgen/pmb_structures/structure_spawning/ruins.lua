local sch = pmb_structures.get_schematic_path


local ruins_max_alt = 10
-- ruins
local ruins_chance_mult = 0.1
if #pmb_biomes.ruin >= 0 then
  minetest.register_decoration({
    deco_type = "schematic",
    place_on = {"group:soil"},
    sidelen = 80,
    noise_params = {
      offset = 0,
      scale = 0.0008 * ruins_chance_mult,
      spread = {x = 1, y = 1, z = 1},
      seed = 87,
      octaves = 2,
      persist = 0,
      lacunarity = 1,
    },
    biomes = pmb_biomes.ruin,
    y_min = 0,
    y_max = ruins_max_alt,
    schematic = sch("ruin_bridge_0"),
    flags = "place_center_x, place_center_z",
    rotation = "random",
    place_offset_y = -8
  })
  -- ruined house 2
  minetest.register_decoration({
    deco_type = "schematic",
    place_on = {"group:soil"},
    sidelen = 80,
    noise_params = {
      offset = 0,
      scale = 0.0008 * ruins_chance_mult,
      spread = {x = 1, y = 1, z = 1},
      seed = 87,
      octaves = 2,
      persist = 0,
      lacunarity = 1,
    },
    biomes = pmb_biomes.ruin,
    y_min = 0,
    y_max = ruins_max_alt,
    schematic = sch("ruin_house_1"),
    flags = "place_center_x, place_center_z, force_placement",
    rotation = "random",
    place_offset_y = -7
  })
  -- ruins
  minetest.register_decoration({
    deco_type = "schematic",
    place_on = {"group:soil"},
    sidelen = 80,
    noise_params = {
      offset = 0,
      scale = 0.0008 * ruins_chance_mult,
      spread = {x = 1, y = 1, z = 1},
      seed = 99,
      octaves = 2,
      persist = 0,
      lacunarity = 1,
    },
    biomes = pmb_biomes.ruin,
    y_min = 0,
    y_max = ruins_max_alt,
    schematic = sch("ruin_house_0"),
    flags = "place_center_x, place_center_z, force_placement",
    rotation = "random",
    place_offset_y = -6
  })
  -- ruins
  minetest.register_decoration({
    deco_type = "schematic",
    place_on = {"group:soil"},
    sidelen = 80,
    noise_params = {
      offset = 0,
      scale = 0.0008 * ruins_chance_mult,
      spread = {x = 1, y = 1, z = 1},
      seed = 67,
      octaves = 2,
      persist = 0,
      lacunarity = 1,
    },
    biomes = pmb_biomes.ruin,
    y_min = 0,
    y_max = ruins_max_alt,
    schematic = sch("ruin_overgrown_bridge_0"),
    flags = "place_center_x, place_center_z, force_placement",
    rotation = "random",
    place_offset_y = -7
  })
  -- ruined HUGE bridge
  minetest.register_decoration({
    deco_type = "schematic",
    place_on = {"group:soil"},
    sidelen = 80,
    noise_params = {
      offset = 0,
      scale = 0.0008 * ruins_chance_mult,
      spread = {x = 1, y = 1, z = 1},
      seed = 34,
      octaves = 2,
      persist = 0,
      lacunarity = 1,
    },
    biomes = pmb_biomes.ruin,
    y_min = -10,
    y_max = ruins_max_alt,
    schematic = sch("ruin_overgrown_bridge_1"),
    flags = "place_center_x, place_center_z, force_placement",
    rotation = "random",
    place_offset_y = -5
  })
end