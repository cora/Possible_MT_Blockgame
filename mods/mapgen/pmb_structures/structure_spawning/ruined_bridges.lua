local sch = pmb_structures.get_schematic_path

if pmb_biomes.registered["beach_bridges"] then
  -- ruins
  minetest.register_decoration({
    deco_type = "schematic",
    place_on = {"pmb_soil:sand", "group:soil"},
    sidelen = 80,
    noise_params = {
      offset = 0,
      scale = 0.003,
      spread = {x = 1, y = 1, z = 1},
      seed = 99,
      octaves = 2,
      persist = 0,
      lacunarity = 1,
    },
    biomes = {"beach_bridges"},
    y_min = -10,
    y_max = -1,
    schematic = sch("ruin_overgrown_bridge_1"),
    flags = "place_center_x, place_center_z, force_placement",
    rotation = "random",
    place_offset_y = -6
  })
  -- ruins
  minetest.register_decoration({
    deco_type = "schematic",
    place_on = {"pmb_soil:sand", "group:soil"},
    sidelen = 80,
    noise_params = {
      offset = 0,
      scale = 0.003,
      spread = {x = 1, y = 1, z = 1},
      seed = 98,
      octaves = 2,
      persist = 0,
      lacunarity = 1,
    },
    biomes = {"beach_bridges"},
    y_min = -10,
    y_max = -1,
    schematic = sch("ruin_overgrown_bridge_0"),
    flags = "place_center_x, place_center_z, force_placement",
    rotation = "random",
    place_offset_y = -4
  })


  -- ruins
  minetest.register_decoration({
    deco_type = "schematic",
    place_on = {"pmb_soil:sand", "group:soil"},
    sidelen = 80,
    noise_params = {
      offset = 0,
      scale = 0.003,
      spread = {x = 10, y = 10, z = 10},
      seed = 99,
      octaves = 2,
      persist = 0,
      lacunarity = 1,
    },
    biomes = {"beach_bridges"},
    y_min = -10,
    y_max = -1,
    schematic = sch("ruin_overgrown_bridge_1"),
    flags = "place_center_x, place_center_z, force_placement",
    rotation = "random",
    place_offset_y = -15
  })
  -- ruins
  minetest.register_decoration({
    deco_type = "schematic",
    place_on = {"pmb_soil:sand", "group:soil"},
    sidelen = 80,
    noise_params = {
      offset = 0,
      scale = 0.003,
      spread = {x = 10, y = 10, z = 10},
      seed = 98,
      octaves = 2,
      persist = 0,
      lacunarity = 1,
    },
    biomes = {"beach_bridges"},
    y_min = -10,
    y_max = -1,
    schematic = sch("ruin_overgrown_bridge_0"),
    flags = "place_center_x, place_center_z, force_placement",
    rotation = "random",
    place_offset_y = -8
  })
end