local sch = pmb_structures.get_schematic_path


-- house
minetest.register_decoration({
  deco_type = "schematic",
  place_on = {"group:soil"},
  sidelen = 80,
  fill_ratio = 0.000006,
  biomes = pmb_biomes.clearing,
  y_min = 0,
  y_max = 200,
  schematic = sch("house_oak_overgrown_0"),
  flags = "place_center_x, place_center_z, force_placement",
  rotation = "random",
  place_offset_y = -6
})

