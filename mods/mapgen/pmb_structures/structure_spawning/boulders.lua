local sch = pmb_structures.get_schematic_path



if pmb_biomes.boulders then
  minetest.register_decoration({
    deco_type = "schematic",
    place_on = {"group:soil"},
    sidelen = 80,
    fill_ratio = 0.0006,
    biomes = pmb_biomes.boulders,
    y_min = -10,
    y_max = 200,
    schematic = sch("boulder_0"),
    flags = "place_center_x, place_center_z, force_placement",
    rotation = "random",
    place_offset_y = -2
  })
  minetest.register_decoration({
    deco_type = "schematic",
    place_on = {"group:soil"},
    sidelen = 80,
    fill_ratio = 0.0006,
    biomes = pmb_biomes.boulders,
    y_min = -10,
    y_max = 200,
    schematic = sch("boulder_1"),
    flags = "place_center_x, place_center_z, force_placement",
    rotation = "random",
    place_offset_y = -2
  })
  minetest.register_decoration({
    deco_type = "schematic",
    place_on = {"group:soil"},
    sidelen = 80,
    fill_ratio = 0.0006,
    biomes = pmb_biomes.boulders,
    y_min = -10,
    y_max = 200,
    schematic = sch("boulder_2"),
    flags = "place_center_x, place_center_z, force_placement",
    rotation = "random",
    place_offset_y = -2
  })
  minetest.register_decoration({
    deco_type = "schematic",
    place_on = {"group:soil"},
    sidelen = 80,
    fill_ratio = 0.006,
    biomes = pmb_biomes.boulders,
    y_min = -10,
    y_max = 200,
    schematic = sch("boulder_2"),
    flags = "place_center_x, place_center_z, force_placement",
    rotation = "random",
    place_offset_y = -4
  })
  minetest.register_decoration({
    deco_type = "schematic",
    place_on = {"group:soil"},
    sidelen = 80,
    fill_ratio = 0.0009,
    biomes = pmb_biomes.boulders,
    y_min = -10,
    y_max = 200,
    schematic = sch("boulder_3"),
    flags = "place_center_x, place_center_z, force_placement",
    rotation = "random",
    place_offset_y = -3
  })
end
