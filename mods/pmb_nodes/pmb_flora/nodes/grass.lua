

local grass_box = {
  type = "fixed",
  fixed = {
    -6/16, -8/16, -6/16,
    6/16, 2/16, 6/16,
  }
}

local function random_rotate(pos)
  local node = minetest.get_node(pos)
  node.param2 = math.random(0,3)
  minetest.set_node(pos, node)
end


local function register_grass(node_name, tiles)
  local name = string.lower(node_name)
  minetest.register_node('pmb_flora:' .. name, {
    description = "Some grass",
    drawtype = "plantlike",
    walkable = false,
    waving = 1,
    paramtype = "light",
    inventory_image = "pmb_" .. name .. ".png",
    wield_image = "pmb_" .. name .. ".png",
    is_ground_content = true,
    sunlight_propagates = true,
    selection_box = grass_box,
    groups = { attached_node = 1, item_grass = 1, dig_immediate = 3, flora = 1, },
    drop = "",
    tiles = tiles,
    buildable_to = true,
    sounds = pmb_sounds.default_plant(),
    on_place = function(itemstack, placer, pointed_thing)
      return pmb_util.only_place_above(itemstack, placer, pointed_thing, {"soil"})
    end,
  })
end

register_grass("grass_0", {"pmb_grass_0.png"})
register_grass("grass_1", {"pmb_grass_1.png"})
register_grass("grass_2", {"pmb_grass_2.png"})
