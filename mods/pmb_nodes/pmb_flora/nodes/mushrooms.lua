


local mushroom_box = {
  type = "fixed",
  fixed = {
    -2/16, -8/16, -2/16,
    2/16, 4/16, 2/16,
  }
}


function pmb_flora.register_mushroom(name)
  local node_name = string.lower(name)
  minetest.register_node('pmb_flora:mushroom_'..node_name, {
    description = name.." mushroom",
    drawtype = "plantlike",
    walkable = false,
    waving = 1,
    paramtype = "light",
    paramtype2 = "meshoptions",
    inventory_image = "pmb_mushroom_"..node_name..".png",
    wield_image = "pmb_mushroom_"..node_name..".png",
    is_ground_content = true,
    sunlight_propagates = true,
    selection_box = mushroom_box,
    groups = { attached_node = 1, item_mushroom_brown = 1, snappy = 3, dig_immediate = 3, flora = 1, },
    tiles = {"pmb_mushroom_"..node_name..".png"},
    sounds = pmb_sounds.default_plant(),
    on_place = function(itemstack, placer, pointed_thing)
      return pmb_util.only_place_above(itemstack, placer, pointed_thing, {"soil"})
    end,
    on_construct = function(pos)
      local node = minetest.get_node(pos)
      node.param2 = 8 -- this makes it randomly offset horizontally
      minetest.swap_node(pos, node, true)
    end,
  })
end

pmb_flora.register_mushroom("Brown")
pmb_flora.register_mushroom("Red")
pmb_flora.register_mushroom("White")
