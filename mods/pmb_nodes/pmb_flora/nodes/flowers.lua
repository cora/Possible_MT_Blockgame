
local flower_box = {
  type = "fixed",
  fixed = {
    -2/16, -8/16, -2/16,
    2/16, 4/16, 2/16,
  }
}

function pmb_flora.register_flower(name, color, node_name)
  node_name = node_name or string.lower(name)
  minetest.register_node('pmb_flora:flower_'..node_name, {
    description = name.." flower",
    drawtype = "plantlike",
    waving = 1,
    paramtype = "light",
    paramtype2 = "meshoptions",
    param2 = 8,
    inventory_image = "pmb_flower_"..node_name..".png",
    wield_image = "pmb_flower_"..node_name..".png",
    walkable = false,
    is_ground_content = true,
    sunlight_propagates = true,
    selection_box = flower_box,
    groups = { attached_node = 1, ["item_flower_"..node_name] = 1, flower = 1, dig_immediate = 3, flora = 1, },
    tiles = {"pmb_flower_"..node_name..".png"},
    sounds = pmb_sounds.default_plant(),
    on_place = function(itemstack, placer, pointed_thing)
      return pmb_util.only_place_above(itemstack, placer, pointed_thing, {"soil"})
    end,
    on_construct = function(pos)
      local node = minetest.get_node(pos)
      node.param2 = 8 -- this makes it randomly offset horizontally
      minetest.swap_node(pos, node, true)
    end,
  })
end

pmb_flora.register_flower("Daisy", "white", "white")
pmb_flora.register_flower("Rose", "red", "red")

