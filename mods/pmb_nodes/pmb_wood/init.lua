
local can_tree_fell = minetest.settings:get_bool("pmb_tree_fell") or false


local function break_me(pos, cause, user, count)
  if not can_tree_fell then return false end
  if cause == "dig"
  and user and user:is_player()
  and player_info[user:get_player_name()] then
    local ctrl = player_info[user:get_player_name()].ctrl
    if ctrl.sneak and ctrl.aux1 then
      minetest.dig_node(pos)
      return true
    end
  end
end



pmb_wood = {}

local log_fuel = 30

local planks_per_log = 4
function pmb_wood.register_planks(name, tiles)
  local node_name = string.lower(name)
  minetest.register_node('pmb_wood:' .. node_name .. '_planks', {
    description = name .. ' planks',
    groups = { oddly_breakable_by_hand = 1, choppy = 1, solid = 1, wood = 1, ["item_"..name.."_planks"] = 1, planks = 1, flammable = 1, fuel = log_fuel/2 },
    tiles = tiles,
    sounds = pmb_sounds.default_wood(),
  })
  minetest.register_craft({
    output = 'pmb_wood:' .. node_name .. '_planks ' .. planks_per_log,
    recipe = {
      {'pmb_wood:' .. node_name .. '_log'},
    },
  })
end


function pmb_wood.on_place_plank(pos, placer, itemstack, pointed_thing)
  if placer:is_player() and player_info[placer:get_player_name()] and player_info[placer:get_player_name()].ctrl.sneak then
    pmb_util.rotate_and_place_against(pos, placer, itemstack, pointed_thing, {offset = 0, copy_same_node = false, vflip = true})
  else
    pmb_util.rotate_and_place_against(pos, placer, itemstack, pointed_thing, {offset = 0, copy_same_node = true,})
  end
end

local plank_box = {
  -8/16,   -8/16, -8/16,
  8/16,   -4/16,  8/16
}
-- local plank_box = {
--   -0.5, -0.5,  4/16,
--   0.5,   0.5,  8/16,
-- }
local plank_per_planks = 2
function pmb_wood.register_plank(name, tiles)
  local node_name = string.lower(name)
  minetest.register_node('pmb_wood:' .. node_name .. '_plank', {
    description = "A single " .. name .. ' plank',
    groups = { oddly_breakable_by_hand = 1, choppy = 1, solid = 1, wood = 1, ["item_"..name.."_plank"] = 1, planks = 1, flammable = 1, fuel = log_fuel/4 },
    drawtype = "nodebox",
    paramtype = "light",
    sunlight_propagates = false,
    paramtype2 = "facedir",
    node_box = {
      type = "fixed",
      fixed = plank_box,
    },
    tiles = tiles,
    sounds = pmb_sounds.default_wood(),
    after_place_node = pmb_wood.on_place_plank,
  })
  minetest.register_craft({
    output = 'pmb_wood:' .. node_name .. '_plank ' .. plank_per_planks,
    recipe = {
      {'pmb_wood:' .. node_name .. '_planks'},
    },
  })
end





function pmb_wood.on_place_post(pos, placer, itemstack, pointed_thing)
  if placer:is_player() and player_info[placer:get_player_name()] and player_info[placer:get_player_name()].ctrl.sneak then
    pmb_util.rotate_and_place_against(pos, placer, itemstack, pointed_thing, {offset = 0, copy_same_node = false, vflip = true})
  else
    pmb_util.rotate_and_place_against(pos, placer, itemstack, pointed_thing, {offset = 1, copy_same_node = true,})
  end
end

local post_box = {
  -8/16,  -8/16, -8/16,
  -0/16,   8/16,  -0/16
}
local post_per_plank = 1
function pmb_wood.register_post(name, tiles)
  local node_name = string.lower(name)
  minetest.register_node('pmb_wood:' .. node_name .. '_post', {
    description = "A single " .. name .. ' post',
    groups = { oddly_breakable_by_hand = 1, choppy = 1, solid = 1, wood = 1, ["item_"..name.."_post"] = 1, planks = 1, flammable = 1, fuel = log_fuel/8 },
    drawtype = "nodebox",
    paramtype = "light",
    paramtype2 = "facedir",
    node_box = {
      type = "fixed",
      fixed = post_box,
    },
    tiles = tiles,
    sounds = pmb_sounds.default_wood(),
    after_place_node = pmb_wood.on_place_post,
  })
  minetest.register_craft({
    output = 'pmb_wood:' .. node_name .. '_post ' .. post_per_plank * 2,
    recipe = {
      {'pmb_wood:' .. node_name .. '_plank', 'pmb_wood:' .. node_name .. '_plank'},
    },
  })
end



-- BEAMS
function pmb_wood.on_place_beam(pos, placer, itemstack, pointed_thing)
  if placer:is_player() and player_info[placer:get_player_name()] and player_info[placer:get_player_name()].ctrl.sneak then
    pmb_util.rotate_and_place_against(pos, placer, itemstack, pointed_thing, {offset = 0, copy_same_node = false, vflip = true})
  else
    pmb_util.rotate_and_place_against(pos, placer, itemstack, pointed_thing, {offset = 0, copy_same_node = true,})
  end
end

local beam_box = {
  -2/16, -8/16, 0,
   2/16,  8/16, 8/16,
}
local beam_per_post = 1
function pmb_wood.register_beam(name, tiles)
  local node_name = string.lower(name)
  minetest.register_node('pmb_wood:' .. node_name .. '_beam', {
    description = "A single " .. name .. ' beam',
    groups = { oddly_breakable_by_hand = 1, choppy = 1, solid = 1, wood = 1, ["item_"..name.."_beam"] = 1, beam = 1, flammable = 1, fuel = log_fuel/8 },
    drawtype = "nodebox",
    paramtype = "light",
    paramtype2 = "facedir",
    node_box = {
      type = "fixed",
      fixed = beam_box,
    },
    tiles = tiles,
    sounds = pmb_sounds.default_wood(),
    after_place_node = pmb_wood.on_place_beam,
  })
  minetest.register_craft({
    output = 'pmb_wood:' .. node_name .. '_beam ' .. beam_per_post * 2,
    recipe = {
      {'pmb_wood:' .. node_name .. '_post', 'pmb_wood:' .. node_name .. '_post'},
    },
  })
end

-- SLABS
function pmb_wood.on_place_slab(pos, placer, itemstack, pointed_thing)
  if placer:is_player() and player_info[placer:get_player_name()] and player_info[placer:get_player_name()].ctrl.sneak then
    pmb_util.rotate_and_place_against(pos, placer, itemstack, pointed_thing, {offset = 0, copy_same_node = true, vflip = true,})
  else
    pmb_util.rotate_and_place_against(pos, placer, itemstack, pointed_thing, {offset = 0, copy_same_node = true,})
  end
end

local slab_box = {
  -8/16,  -8/16, -8/16,
   8/16,   0,     8/16
}
local slab_per_planks = 6
function pmb_wood.register_slab(name, tiles)
  local node_name = string.lower(name)
  minetest.register_node('pmb_wood:' .. node_name .. '_slab', {
    description = "A single " .. name .. ' slab',
    groups = { oddly_breakable_by_hand = 1, choppy = 1, solid = 1, wood = 1, ["item_"..name.."_slab"] = 1, slab = 1, flammable = 1, fuel = 10 },
    drawtype = "nodebox",
    paramtype = "light",
    sunlight_propagates = false,
    paramtype2 = "facedir",
    node_box = {
      type = "fixed",
      fixed = slab_box,
    },
    tiles = tiles,
    sounds = pmb_sounds.default_wood(),
    after_place_node = pmb_wood.on_place_slab,
  })
  minetest.register_craft({
    output = 'pmb_wood:' .. node_name .. '_slab ' .. slab_per_planks,
    recipe = {
      {'pmb_wood:' .. node_name .. '_planks', 'pmb_wood:' .. node_name .. '_planks', 'pmb_wood:' .. node_name .. '_planks'},
    },
  })
end




function pmb_wood.on_place_log(pos, placer, itemstack, pointed_thing)
  pmb_util.rotate_and_place_against(pos, placer, itemstack, pointed_thing, {offset = 0})
end

function pmb_wood.register_log(name, tiles)
  local node_name = string.lower(name)
  minetest.register_node('pmb_wood:' .. node_name .. '_log', {
    description = name .. ' log',
    groups = { oddly_breakable_by_hand = 2, choppy = 1, solid = 1, wood = 1, ["item_"..name.."_log"] = 1, wood_log = 1, flammable = 1, fuel = log_fuel },
    tiles = tiles,
    sounds = pmb_sounds.default_wood(),
    paramtype2 = "facedir",
    after_place_node = pmb_wood.on_place_log,
    _on_node_update = break_me,
  })
end

function pmb_wood.register_leaves(name, tiles, tiles_simple)
  local node_name = string.lower(name)
  minetest.register_node('pmb_wood:' .. node_name .. '_leaves', {
    description = name .. ' leaves',
    drawtype = "allfaces_optional",
    waving = 1,
    tiles = tiles,
    special_tiles = tiles_simple,
    paramtype = "light",
    groups = { oddly_breakable_by_hand = 1, solid = 1, leaves = 1, leaf_decay = 1, ["item_"..name.."_leaves"] = 1, flammable = 1 },
    is_ground_content = false,
    after_place_node = pmb_util.rotate_and_place_against,
    drop = {
      max_items = 1,
      items = {
        {
        	items = {'pmb_trees:' .. node_name .. '_sapling'},
        	rarity = 20,
        },
        {
        	items = {'pmb_items:stick'},
        	rarity = 10,
        },
      },
    },
  sounds = pmb_sounds.default_plant(), -- #TODO add leaf sounds
  _on_node_update = break_me,
})
end


pmb_wood.register_log(   "Oak", {'pmb_wood_oak_log_top.png', 'pmb_wood_oak_log_top.png', 'pmb_wood_oak_log_side.png'})
pmb_wood.register_leaves("Oak", {'pmb_wood_oak_leaves.png'}, {'pmb_wood_oak_leaves_opaque.png'})
pmb_wood.register_planks("Oak", {'pmb_wood_oak_planks.png'})
pmb_wood.register_slab(  "Oak", {'pmb_wood_oak_planks.png'})
pmb_wood.register_beam(  "Oak", {'pmb_wood_oak_plank.png'})
pmb_wood.register_plank( "Oak", {'pmb_wood_oak_plank.png'})
pmb_wood.register_post(  "Oak", {'pmb_wood_oak_plank.png'})


pmb_wood.register_log(   "Ash", {'pmb_wood_ash_log_top.png', 'pmb_wood_ash_log_top.png', 'pmb_wood_ash_log_side.png'})
pmb_wood.register_leaves("Ash", {'pmb_wood_ash_leaves.png'}, {'pmb_wood_ash_leaves_opaque.png'})
pmb_wood.register_planks("Ash", {'pmb_wood_ash_planks.png'})
pmb_wood.register_slab(  "Ash", {'pmb_wood_ash_planks.png'})
pmb_wood.register_plank( "Ash", {'pmb_wood_ash_plank.png'})
pmb_wood.register_post(  "Ash", {'pmb_wood_ash_plank.png'})
pmb_wood.register_beam(  "Ash", {'pmb_wood_ash_plank.png'})


pmb_wood.register_log(   "Spruce", {'pmb_wood_spruce_log_top.png', 'pmb_wood_spruce_log_top.png', 'pmb_wood_spruce_log_side.png'})
pmb_wood.register_leaves("Spruce", {'pmb_wood_spruce_leaves.png'}, {'pmb_wood_spruce_leaves_opaque.png'})
pmb_wood.register_planks("Spruce", {'pmb_wood_spruce_planks.png'})
pmb_wood.register_slab(  "Spruce", {'pmb_wood_spruce_planks.png'})
pmb_wood.register_plank( "Spruce", {'pmb_wood_spruce_plank.png'})
pmb_wood.register_post(  "Spruce", {'pmb_wood_spruce_plank.png'})
pmb_wood.register_beam(  "Spruce", {'pmb_wood_spruce_plank.png'})




-- CHARCOAL
minetest.register_node('pmb_wood:charcoal_block', {
  description = "Charcoal_block",
  groups = { item_charcoal_block = 1, oddly_breakable_by_hand = 3, solid = 1, flammable = 1, fuel = 60 },
  tiles = {"pmb_charcoal_block.png"},
  sounds = pmb_sounds.default_stone(),
})

minetest.register_node('pmb_wood:hot_coals', {
  description = "Hot coals. Ouch.",
  groups = { item_hot_coals = 1, oddly_breakable_by_hand = 3, solid = 1, flammable = 1, fuel = 60 },
  tiles = {"pmb_hot_coals.png"},
  drop = "pmb_wood:charcoal_block",
  sounds = pmb_sounds.default_stone(),
  light_source = 6,
})

-- cool down hot coals or heat them up
minetest.register_abm({
  nodenames = {"pmb_wood:hot_coals"},
  neighbours = {"air"},
  interval = 3.0,
  chance = 20,
  action = function(pos, node)
    local above = minetest.get_node(vector.offset(pos, 0, 1, 0))
    if minetest.get_item_group(above.name, "fire") == 0 then
      minetest.set_node(pos, {name="pmb_wood:charcoal_block"})
    end
  end,
})
minetest.register_abm({
  nodenames = {"pmb_wood:hot_coals"},
  neighbours = {"pmb_fire:fire"},
  interval = 3.0,
  chance = 10,
  action = function(pos, node)
    minetest.set_node(pos, {name="pmb_wood:hot_coals"})
  end,
})