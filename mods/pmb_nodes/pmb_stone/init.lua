local mod_name = minetest.get_current_modname()
local mod_path = minetest.get_modpath(mod_name)
local S = minetest.get_translator(mod_name)


pmb_stone = {}


minetest.register_node('pmb_stone:stone', {
  description = 'Stone',
  groups = { item_stone = 1, oddly_breakable_by_hand = 3, solid = 1, cracky = 1, stone = 1,  silk_touchable = 1 },
  tiles = { 'pmb_stone_stone.png' },
  drop = 'pmb_stone:cobble',
  sounds = pmb_sounds.default_stone(),
})
minetest.register_node('pmb_stone:granite', {
  description = 'Granite',
  groups = { item_granite = 1, oddly_breakable_by_hand = 3, solid = 1, cracky = 1,  stone = 1,  },
  tiles = { 'pmb_stone_granite.png' },
  sounds = pmb_sounds.default_stone(),
})
minetest.register_node('pmb_stone:cobble', {
  description = 'Cobbled Stone',
  groups = { item_cobble = 1, oddly_breakable_by_hand = 3, solid = 1, cracky = 1,  stone = 1,  },
  tiles = { 'pmb_stone_cobble.png' },
  sounds = pmb_sounds.default_stone(),
})
minetest.register_node('pmb_stone:cobble_moss_1', {
  description = 'Cobbled Stone with some Moss',
  groups = { item_cobble_moss_1 = 1, oddly_breakable_by_hand = 3, solid = 1, cracky = 1,  stone = 1,  },
  tiles = { 'pmb_stone_cobble_moss_1.png' },
  drop = 'pmb_stone:cobble_moss_1',
  sounds = pmb_sounds.default_stone(),
})
minetest.register_node('pmb_stone:cobble_moss_2', {
  description = 'Cobbled Stone Moss',
  groups = { item_cobble_moss_2 = 1, oddly_breakable_by_hand = 3, solid = 1, cracky = 1,  stone = 1,  },
  tiles = { 'pmb_stone_cobble_moss_2.png' },
  drop = 'pmb_stone:cobble_moss_2',
  sounds = pmb_sounds.default_stone(),
})


-- SLABS
function pmb_stone.on_place_slab(pos, placer, itemstack, pointed_thing)
  if placer:is_player() and player_info[placer:get_player_name()] and player_info[placer:get_player_name()].ctrl.sneak then
    pmb_util.rotate_and_place_from(pos, placer, itemstack, pointed_thing, {offset = 0, copy_same_node = false,})
  else
    pmb_util.rotate_and_place_against(pos, placer, itemstack, pointed_thing, {offset = 0, copy_same_node = true,})
  end
end

local slab_box = {
  -8/16,  -8/16, -8/16,
   8/16,   0,     8/16
}
local slab_count = 6
function pmb_stone.register_slab(name, tiles)
  local node_name = string.lower(name)
  minetest.register_node('pmb_stone:' .. node_name .. '_slab', {
    description = "A single " .. name .. ' slab',
    groups = { cracky = 1, oddly_breakable_by_hand = 3, solid = 1, stone = 1, ["item_"..name.."_slab"] = 1, slab = 1, },
    drawtype = "nodebox",
    paramtype = "light",
    sunlight_propagates = false,
    paramtype2 = "facedir",
    node_box = {
      type = "fixed",
      fixed = slab_box,
    },
    tiles = tiles,
    sounds = pmb_sounds.default_stone(),
    after_place_node = pmb_stone.on_place_slab,
  })
  minetest.register_craft({
    output = 'pmb_stone:' .. node_name .. '_slab ' .. slab_count,
    recipe = {
      {'pmb_stone:' .. node_name .. '', 'pmb_stone:' .. node_name .. '', 'pmb_stone:' .. node_name .. ''},
    },
  })
end

pmb_stone.register_slab("Stone", {"pmb_stone_stone.png"})
pmb_stone.register_slab("Cobble", {"pmb_stone_cobble.png"})
pmb_stone.register_slab("Granite", {"pmb_stone_granite.png"})





minetest.register_node('pmb_stone:stone_iron_ore', {
  description = 'Iron ore',
  groups = { item_iron_ore = 1, solid = 1, cracky = 2, oddly_breakable_by_hand = 3, stone = 1,  silk_touchable = 1 },
  tiles = { 'pmb_stone_stone.png^pmb_iron_overlay.png' },
  sounds = pmb_sounds.default_stone(),
  drop = {
    max_items = 1,
    items = {
      {
        items = {'pmb_items:iron_nugget'},
      },
    },
  },
})





minetest.register_node('pmb_stone:stone_tin_ore', {
  description = 'Tin ore',
  groups = { item_tin_ore = 1, solid = 1, cracky = 2, oddly_breakable_by_hand = 3, stone = 1,  silk_touchable = 1 },
  tiles = { 'pmb_stone_stone.png^pmb_tin_overlay.png' },
  sounds = pmb_sounds.default_stone(),
  drop = {
    max_items = 1,
    items = {
      {
        items = {'pmb_items:tin_nugget'},
      },
    },
  },
})





minetest.register_node('pmb_stone:stone_copper_ore', {
  description = 'Copper ore',
  groups = { item_copper_ore = 1, solid = 1, cracky = 2, oddly_breakable_by_hand = 3, stone = 1,  silk_touchable = 1 },
  tiles = { 'pmb_stone_stone.png^pmb_copper_overlay.png' },
  sounds = pmb_sounds.default_stone(),
  drop = {
    max_items = 1,
    items = {
      {
        items = {'pmb_items:copper_nugget'},
      },
    },
  },
})





minetest.register_node('pmb_stone:stone_lapis_ore', {
  description = 'Lapis lazuli ore',
  groups = { item_lapis_ore = 1, solid = 1, cracky = 3, oddly_breakable_by_hand = 3, stone = 1,  silk_touchable = 1 },
  tiles = { 'pmb_stone_stone.png^pmb_lapis_overlay.png' },
  sounds = pmb_sounds.default_stone(),
  drop = {
    max_items = 7,
    items = {
      {
        items = {'pmb_items:lapis_lazuli 2'},
      },
      {
        items = {'pmb_items:lapis_lazuli 2'},
        rarity = 2
      },
      {
        items = {'pmb_items:lapis_lazuli 3'},
        rarity = 6
      },
    },
  },
})





minetest.register_node('pmb_stone:stone_diamond_ore', {
  description = 'Diamond ore',
  groups = { item_diamond_ore = 1, solid = 1, cracky = 3, oddly_breakable_by_hand = 3, stone = 1,  silk_touchable = 1 },
  tiles = { 'pmb_stone_stone.png^pmb_diamond_overlay.png' },
  sounds = pmb_sounds.default_stone(),
  drop = {
    max_items = 1,
    items = {
      {
        items = {'pmb_items:diamond'},
      },
    },
  },
})



minetest.register_node('pmb_stone:stone_coal_ore', {
  description = 'Coal ore',
  groups = { item_coal_ore = 1, solid = 1, cracky = 2, oddly_breakable_by_hand = 3, stone = 1,  silk_touchable = 1 },
  tiles = { 'pmb_stone_stone.png^pmb_coal_overlay.png' },
  sounds = pmb_sounds.default_stone(),
  drop = {
    max_items = 1,
    items = {
      {
        items = {'pmb_items:coal'},
      },
    },
  },
})





minetest.register_node('pmb_stone:bedrock', {
  description = 'Bedrock',
  groups = { solid = 1, },
  tiles = { 'pmb_bedrock.png' },
  drop = '',
  sounds = pmb_sounds.default_stone(),
  sunlight_propagates = true,
  diggable = false,
  on_dig = function(pos, node, digger)
    local name = (digger:is_player() and digger:get_player_name()) or dump(digger)
    minetest.log("\n\n[!!!] Bedrock was dug at " .. tostring(pos) .. " by " .. name .. "\n\n")
  end
})

