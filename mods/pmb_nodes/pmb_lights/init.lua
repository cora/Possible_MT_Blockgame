
local torch = {
  width = 2 / 16,
  height = 10 / 16,
}

pmb_lights = {}

function pmb_lights.on_place_torch(pos, placer, itemstack, pointed_thing)
  pmb_util.rotate_and_place_against(pos, placer, itemstack, pointed_thing, {offset = 0, copy_same_node = true, vflip = false, no_yaw = true})
end


minetest.register_node("pmb_lights:torch", {
    description = "torch",
    light_source = 14,
		drawtype = "nodebox",
		walkable = false,
    paramtype2 = "facedir",
    tiles = {"pmb_lights_torch_top.png", "pmb_lights_torch.png",},
		node_box = {
			type = "fixed",
			fixed = {
        -torch.width/2, -0.5, -torch.width/2,
        torch.width/2, -0.5 + torch.height, torch.width/2,},
		},
    groups = { dig_immediate = 3, deco_block = 1, },
    sounds = pmb_sounds.default_stone(),
    after_place_node = pmb_lights.on_place_torch,
})

minetest.register_craft({
  output = "pmb_lights:torch 4",
  recipe = {
    {"group:coal"},
    {"pmb_items:stick"},
  },
})