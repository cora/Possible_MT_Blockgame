

minetest.register_node('pmb_soil:sand', {
  description = 'Sand',
  groups = { item_sand = 1, solid = 1, oddly_breakable_by_hand = 2, crumbly = 1, falling_node = 1, },
  tiles = { 'pmb_sand.png' },
  sounds = pmb_sounds.default_soil(),
})
minetest.register_node('pmb_soil:gravel', {
  description = 'Gravel',
  groups = { item_sand = 1, solid = 1, oddly_breakable_by_hand = 2, crumbly = 1, falling_node = 1, },
  tiles = { 'pmb_gravel.png' },
  sounds = pmb_sounds.default_soil(),
})
minetest.register_node('pmb_soil:dirt', {
  description = 'Dirt',
  groups = { item_dirt = 1, solid = 1, oddly_breakable_by_hand = 2, crumbly = 1, soil = 1, dirt = 1, grass_can_grow = 1, },
  tiles = { 'pmb_dirt.png' },
  sounds = pmb_sounds.default_soil(),
})

---------------
-- NORMAL GRASS
---------------
minetest.register_node('pmb_soil:dirt_with_grass', {
  description = 'Dirt with Grass',
  groups = { item_dirt_with_grass = 1, solid = 1, topsoil = 1,
    oddly_breakable_by_hand = 2, crumbly = 1, soil = 1, dirt = 1, silk_touchable = 1, spreads_to_dirt = 1, },
  tiles = {
    'pmb_grass.png',
    'pmb_dirt.png',
    'pmb_grass_side.png',
  },
  drop = 'pmb_soil:dirt',
  sounds = pmb_sounds.default_soil(),
})
---------------
-- FOREST GRASS
---------------
minetest.register_node('pmb_soil:forest_grass', {
  description = 'Dirt with Grass',
  groups = { item_forest_grass = 1, solid = 1, topsoil = 1,
    oddly_breakable_by_hand = 2, crumbly = 1, soil = 1, dirt = 1, silk_touchable = 1, },
  tiles = {
    'pmb_forest_grass.png',
    'pmb_dirt.png',
    'pmb_forest_grass_side.png',
  },
  drop = 'pmb_soil:dirt',
  sounds = pmb_sounds.default_soil(),
})

minetest.register_node('pmb_soil:forest_dirt', {
  description = 'Dirt in forest.',
  groups = { item_forest_dirt = 1, solid = 1, topsoil = 1,
    oddly_breakable_by_hand = 2, crumbly = 1, soil = 1, dirt = 1, silk_touchable = 1, spreads_to_dirt = 1, },
  tiles = {
    'pmb_forest_dirt.png',
    'pmb_dirt.png',
    'pmb_forest_dirt_side.png',
  },
  drop = 'pmb_soil:dirt',
  sounds = pmb_sounds.default_soil(),
})

---------------
-- SNOW GRASS
---------------
minetest.register_node('pmb_soil:grass_snow', {
  description = 'Grass with snow',
  groups = { item_grass_snow = 1, solid = 1, topsoil = 1,
    oddly_breakable_by_hand = 2, crumbly = 1, soil = 1, dirt = 1, },
  tiles = {
    'pmb_snow.png',
    'pmb_dirt.png',
    'pmb_dirt.png^pmb_grass_snow_side.png',
  },
  drop = 'pmb_soil:dirt',
  sounds = pmb_sounds.default_snow(),
})

---------------
-- SNOW BLOCK
---------------
minetest.register_node('pmb_soil:snow', {
  description = 'Snow block',
  groups = { item_snow = 1, solid = 1,
    oddly_breakable_by_hand = 1, crumbly = 1, snow = 1, cold = 1, silk_touchable = 1, },
  tiles = {
    'pmb_snow.png',
  },
  drop = 'pmb_soil:snow',
  sounds = pmb_sounds.default_snow(),
})


minetest.register_alias('pmb_nodes_surface:sand', 'pmb_soil:sand')
minetest.register_alias('pmb_nodes_surface:dirt', 'pmb_soil:dirt')
