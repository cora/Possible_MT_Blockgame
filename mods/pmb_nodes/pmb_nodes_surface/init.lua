pmb_nodes_surface = {}

minetest.register_node('pmb_nodes_surface:water_source', {
  description = 'Water Source',
  groups = { water_source = 1, oddly_breakable_by_hand = 3, liquid = 1, water = 1, flowing = 0 },

  tiles = { {
      name = 'pmb_surface_water.png^[opacity:190',
      backface_culling = false,
  } },
  use_texture_alpha = 'blend',
  drawtype = 'liquid',
  paramtype = 'light',
  waving = 3,

  walkable = false,
  liquid_move_physics = true,
  move_resistance = 1,
  pointable = false,
  diggable = false,
  buildable_to = true,
  liquidtype = "source",
  liquid_viscosity = 0,
  liquid_range = 8,
  liquid_alternative_source = "pmb_nodes_surface:water_source",
  liquid_alternative_flowing = "pmb_nodes_surface:water_flowing",
})
minetest.register_node('pmb_nodes_surface:water_flowing', {
  description = 'Water Source',
  groups = { water_flowing = 1, oddly_breakable_by_hand = 3, liquid = 1, water = 1, flowing = 1 },

  special_tiles = {
    {
      name = 'pmb_surface_water.png^[opacity:190',
      backface_culling = false,
    },
    {
      name = 'pmb_surface_water.png^[opacity:190',
      backface_culling = true,
    }
  },
  tiles = {'pmb_surface_water.png^[opacity:190'},
  use_texture_alpha = 'blend',
	drawtype = "flowingliquid",

  paramtype = 'light',
	paramtype2 = "flowingliquid",
  waving = 3,

  walkable = false,
  liquid_move_physics = true,
  move_resistance = 1,
  pointable = false,
  diggable = false,
  buildable_to = true,
	liquidtype = "flowing",
  liquid_viscosity = 0,
  -- liquid_range = 8,
  liquid_alternative_source = "pmb_nodes_surface:water_source",
  liquid_alternative_flowing = "pmb_nodes_surface:water_flowing",
})





minetest.register_node('pmb_nodes_surface:river_water_source', {
  description = 'Water Source',
  groups = { water_source = 1, oddly_breakable_by_hand = 3, liquid = 1, water = 1, flowing = 0 },

  tiles = { {
      name = 'pmb_surface_water.png^[opacity:190',
      backface_culling = false,
  } },
  use_texture_alpha = 'blend',
  drawtype = 'liquid',
  paramtype = 'light',
  waving = 3,

  walkable = false,
  liquid_move_physics = true,
  move_resistance = 1,
  pointable = false,
  diggable = false,
  buildable_to = true,
  liquidtype = "source",
  liquid_viscosity = 0,
  liquid_range = 8,
})
minetest.register_node('pmb_nodes_surface:riverwater_flowing', {
  description = 'Water Source',
  groups = { water_flowing = 1, oddly_breakable_by_hand = 3, liquid = 1, water = 1, flowing = 1 },

  special_tiles = {
    {
      name = 'pmb_surface_water.png^[opacity:190',
      backface_culling = false,
    },
    {
      name = 'pmb_surface_water.png^[opacity:190',
      backface_culling = true,
    }
  },
  tiles = {'pmb_surface_water.png^[opacity:190'},
  use_texture_alpha = 'blend',
	drawtype = "flowingliquid",

  paramtype = 'light',
	paramtype2 = "flowingliquid",
  waving = 3,

  walkable = false,
  liquid_move_physics = true,
  move_resistance = 1,
  pointable = false,
  diggable = false,
  buildable_to = true,
	liquidtype = "flowing",
  liquid_viscosity = 0,
  -- liquid_range = 2,
  liquid_alternative_source = "pmb_nodes_surface:river_water_source",
  liquid_alternative_flowing = "pmb_nodes_surface:river_water_flowing",
})








minetest.register_node('pmb_nodes_surface:lava_source', {
  description = 'Lava Source',
  groups = { lava_source = 1, oddly_breakable_by_hand = 3, liquid = 1, lava = 1, flowing = 0 },

  tiles = {'pmb_surface_lava.png'},
  use_texture_alpha = 'blend',
  drawtype = 'liquid',
  paramtype = 'light',
	light_source = 13,
  waving = 3,

  walkable = false,
  liquid_move_physics = true,
  move_resistance = 1,
  pointable = false,
  diggable = false,
  buildable_to = true,
  liquidtype = "source",
  liquid_renewable = false,
  liquid_viscosity = 3,
  liquid_range = 4,
  damage_per_second = 1, -- #TODO damage
  liquid_alternative_source = "pmb_nodes_surface:lava_source",
  liquid_alternative_flowing = "pmb_nodes_surface:lava_flowing",
})

minetest.register_node('pmb_nodes_surface:lava_flowing', {
  description = 'Water Source',
  groups = { lava_flowing = 1, oddly_breakable_by_hand = 3, liquid = 1, lava = 1, flowing = 1 },

  special_tiles = {
    {
      name = 'pmb_surface_lava.png',
      backface_culling = false,
    },
    {
      name = 'pmb_surface_lava.png',
      backface_culling = true,
    }
  },
  tiles = {'pmb_surface_lava.png'},
  -- use_texture_alpha = 'blend',
	drawtype = "flowingliquid",
  paramtype = 'light',
	paramtype2 = "flowingliquid",
	light_source = 13,
  waving = 3,

  walkable = false,
  liquid_move_physics = true,
  move_resistance = 1,
  pointable = false,
  diggable = false,
  buildable_to = true,
  liquidtype = "flowing",
  liquid_renewable = false,
  liquid_viscosity = 6,
  liquid_range = 4,
  damage_per_second = 1, -- #TODO damage
  liquid_alternative_source = "pmb_nodes_surface:lava_source",
  liquid_alternative_flowing = "pmb_nodes_surface:lava_flowing",
})
