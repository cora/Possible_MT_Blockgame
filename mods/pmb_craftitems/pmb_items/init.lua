local mod_name = minetest.get_current_modname()
local mod_path = minetest.get_modpath(mod_name)

pmb_items = {}

dofile(mod_path .. DIR_DELIM .. "items" .. DIR_DELIM .. "wood.lua")
dofile(mod_path .. DIR_DELIM .. "items" .. DIR_DELIM .. "ores.lua")
dofile(mod_path .. DIR_DELIM .. "items" .. DIR_DELIM .. "bars.lua")
