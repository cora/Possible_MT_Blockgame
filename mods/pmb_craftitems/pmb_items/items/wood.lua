local mod_name = minetest.get_current_modname()
local mod_path = minetest.get_modpath(mod_name)
local S = minetest.get_translator(mod_name)


minetest.register_craftitem("pmb_items:stick", {
	description = S("A stick"),
	inventory_image = "pmb_stick.png",
	groups = { item_stick = 1, craftitem = 1, wood = 1, fuel = 3 },
})
minetest.register_craft({
  output = "pmb_items:stick 4",
  recipe = {
    {"group:planks"},
    {"group:planks"},
  },
})
minetest.register_craft({
  output = "pmb_items:stick 1",
  recipe = {
    {"group:leaves"},
  },
})

minetest.register_craftitem("pmb_items:charcoal", {
	description = S("A piece of charcoal"),
	inventory_image = "pmb_charcoal.png",
	groups = { item_charcoal = 1, craftitem = 1, fuel = 30, coal = 1 },
})
minetest.register_craft({
  output = "pmb_items:charcoal 4",
  recipe = {
    {"pmb_wood:charcoal_block"},
  },
})


