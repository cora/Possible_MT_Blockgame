local mod_name = minetest.get_current_modname()
local mod_path = minetest.get_modpath(mod_name)
local S = minetest.get_translator(mod_name)



minetest.register_craftitem("pmb_items:iron_nugget", {
	description = S("Some iron ore"),
	inventory_image = "pmb_iron_nugget.png",
	groups = { item_iron_nugget = 1, craftitem = 1, iron = 1, nugget = 1, iron_nugget = 1, },
})




minetest.register_craftitem("pmb_items:tin_nugget", {
	description = S("Some tin ore"),
	inventory_image = "pmb_tin_nugget.png",
	groups = { item_tin_nugget = 1, craftitem = 1, tin = 1, nugget = 1, tin_nugget = 1, },
})




minetest.register_craftitem("pmb_items:copper_nugget", {
	description = S("Some copper ore"),
	inventory_image = "pmb_copper_nugget.png",
	groups = { item_copper_nugget = 1, craftitem = 1, copper = 1, nugget = 1, copper_nugget = 1, },
})




minetest.register_craftitem("pmb_items:bronze_nugget", {
	description = S("Some bronze to be smelted"),
	inventory_image = "pmb_bronze_nugget.png",
	groups = { item_bronze_nugget = 1, craftitem = 1, bronze = 1, nugget = 1, bronze_nugget = 1, },
})




minetest.register_craftitem("pmb_items:lapis_lazuli", {
	description = S("Some lapis lazuli"),
	inventory_image = "pmb_lapis.png",
	groups = { item_lapis_lazuli = 1, craftitem = 1, lapis = 1, lapis_lazuli = 1, },
})




minetest.register_craftitem("pmb_items:diamond", {
	description = S("Some diamond ore"),
	inventory_image = "pmb_diamond.png",
	groups = { item_diamond = 1, craftitem = 1, diamond = 1,},
})




minetest.register_craftitem("pmb_items:coal", {
	description = S("Some coal ore"),
	inventory_image = "pmb_coal.png",
	groups = { item_coal = 1, craftitem = 1, fuel = 30, coal = 1 },
})


