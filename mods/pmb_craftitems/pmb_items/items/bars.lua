local mod_name = minetest.get_current_modname()
local mod_path = minetest.get_modpath(mod_name)
local S = minetest.get_translator(mod_name)


local bar_cook_time = 3

minetest.register_craftitem("pmb_items:iron_bar", {
	description = S("Iron bar"),
	inventory_image = "pmb_iron_bar.png",
	groups = { item_iron_bar = 1, craftitem = 1, iron = 1, iron_bar = 1, },
})
pmb_cooking.register_cooking({
  raw = "pmb_items:iron_nugget",
  cooked = "pmb_items:iron_bar",
  cook_time = bar_cook_time,
  groups = {"cooker_crucible"},
})




minetest.register_craftitem("pmb_items:tin_bar", {
	description = S("Tin bar"),
	inventory_image = "pmb_tin_bar.png",
	groups = { item_tin_bar = 1, craftitem = 1, tin = 1, tin_bar = 1, },
})
pmb_cooking.register_cooking({
  raw = "pmb_items:tin_nugget",
  cooked = "pmb_items:tin_bar",
  cook_time = bar_cook_time,
  groups = {"cooker_crucible"},
})




minetest.register_craftitem("pmb_items:copper_bar", {
	description = S("Copper bar"),
	inventory_image = "pmb_copper_bar.png",
	groups = { item_copper_bar = 1, craftitem = 1, copper = 1, copper_bar = 1, },
})
pmb_cooking.register_cooking({
  raw = "pmb_items:copper_nugget",
  cooked = "pmb_items:copper_bar",
  cook_time = bar_cook_time,
  groups = {"cooker_crucible"},
})




minetest.register_craftitem("pmb_items:bronze_bar", {
	description = S("Bronze bar"),
	inventory_image = "pmb_bronze_bar.png",
	groups = { item_bronze_bar = 1, craftitem = 1, bronze = 1, bronze_bar = 1, },
})
pmb_cooking.register_cooking({
  raw = "pmb_items:bronze_nugget",
  cooked = "pmb_items:bronze_bar",
  cook_time = bar_cook_time,
  groups = {"cooker_crucible"},
})
minetest.register_craft({
  output = "pmb_items:bronze_nugget 2",
  type = "shapeless",
  recipe = { "pmb_items:copper_nugget", "pmb_items:tin_nugget"
  },
})


