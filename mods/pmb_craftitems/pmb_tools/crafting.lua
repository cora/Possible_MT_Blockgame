local materials = {
    wooden = "group:planks",
    stone = "pmb_stone:cobble",
    bronze = "pmb_items:bronze_bar",
    iron = "pmb_items:iron_bar",
    diamond = "pmb_items:diamond",
}

-- Pickaxes
for mat, item in pairs(materials) do
    minetest.register_craft({
        output = "pmb_tools:" .. mat .. "_pickaxe",
        recipe = {
            { item, item, item },
            { "", "pmb_items:stick", "" },
            { "", "pmb_items:stick", "" },
        },
    })

    minetest.register_craft({
        output = "pmb_tools:" .. mat .. "_axe",
        recipe = {
            { item, item },
            { item, "pmb_items:stick" },
            { "", "pmb_items:stick" },
        },
    })

    minetest.register_craft({
        output = "pmb_tools:" .. mat .. "_axe",
        recipe = {
            { item, item },
            { "pmb_items:stick", item },
            { "pmb_items:stick", "" },
        },
    })

    minetest.register_craft({
        output = "pmb_tools:"..mat.."_shovel",
        recipe = {
            { item },
            { "pmb_items:stick" },
            { "pmb_items:stick" },
        },
    })
end