local mod_name = minetest.get_current_modname()
local mod_path = minetest.get_modpath(mod_name)
local S = minetest.get_translator(mod_name)




---------------------------------------------------
-----------------------WOOD------------------------
---------------------------------------------------
-- Pickaxes
minetest.register_tool("pmb_tools:wooden_pickaxe", {
    description = S("A wooden pickaxe"),
    inventory_image = "pmb_wooden_pickaxe.png",
    tool_capabilities = {
        groupcaps = {
            cracky = {
                times = { 1 },
                uses = 32,
            },
        },
    },
    groups = { pickaxe = 1 },
})

-- Axes
minetest.register_tool("pmb_tools:wooden_axe", {
    description = S("A wooden axe"),
    inventory_image = "pmb_wooden_axe.png",
    tool_capabilities = {
        groupcaps = {
            choppy = {
                times = { 0.7 },
                uses = 32,
            },
        },
    },
    groups = { axe = 1 },
})

-- Shovels
minetest.register_tool("pmb_tools:wooden_shovel", {
    description = S("A wooden shovel"),
    inventory_image = "pmb_wooden_shovel.png",
    tool_capabilities = {
        groupcaps = {
            crumbly = {
                times = { 0.5 },
                uses = 32,
            },
        },
    },
    groups = { shovel = 1 },
})

---------------------------------------------------
-----------------------STONE-----------------------
---------------------------------------------------

minetest.register_tool("pmb_tools:stone_pickaxe", {
    description = S("A stone pickaxe"),
    inventory_image = "pmb_stone_pickaxe.png",
    tool_capabilities = {
        groupcaps = {
            cracky = {
                times = { .7, 1 },
                uses = 64,
            },
        },
    },
    groups = { pickaxe = 2 },
})

minetest.register_tool("pmb_tools:stone_axe", {
    description = S("A stone axe"),
    inventory_image = "pmb_stone_axe.png",
    tool_capabilities = {
        groupcaps = {
            choppy = {
                times = { 0.4 },
                uses = 64,
            },
        },
    },
    groups = { axe = 2 },
})

minetest.register_tool("pmb_tools:stone_shovel", {
    description = S("A stone shovel"),
    inventory_image = "pmb_stone_shovel.png",
    tool_capabilities = {
        groupcaps = {
            crumbly = {
                times = { 0.35 },
                uses = 64,
            },
        },
    },
    groups = { shovel = 2 },
})

---------------------------------------------------
-----------------------IRON------------------------
---------------------------------------------------

minetest.register_tool("pmb_tools:iron_pickaxe", {
    description = S("An iron pickaxe"),
    inventory_image = "pmb_iron_pickaxe.png",
    tool_capabilities = {
        groupcaps = {
            cracky = {
                times = { .5, .7, 1 },
                uses = 192,
            },
        },
    },
    groups = { pickaxe = 3 },
})

minetest.register_tool("pmb_tools:iron_axe", {
    description = S("An iron axe"),
    inventory_image = "pmb_iron_axe.png",
    tool_capabilities = {
        groupcaps = {
            choppy = {
                times = { 0.25 },
                uses = 192,
            },
        },
    },
    groups = { axe = 3 },
})

minetest.register_tool("pmb_tools:iron_shovel", {
    description = S("An iron shovel"),
    inventory_image = "pmb_iron_shovel.png",
    tool_capabilities = {
        groupcaps = {
            crumbly = {
                times = { 0.225 },
                uses = 192,
            },
        },
    },
    groups = { shovel = 3 },
})

---------------------------------------------------
----------------------BRONZE-----------------------
---------------------------------------------------

minetest.register_tool("pmb_tools:bronze_pickaxe", {
    description = S("An bronze pickaxe"),
    inventory_image = "pmb_bronze_pickaxe.png",
    tool_capabilities = {
        groupcaps = {
            cracky = {
                times = { 0.5, 0.7, 1 },
                uses = 128,
            },
        },
    },
    groups = { pickaxe = 3 },
})

minetest.register_tool("pmb_tools:bronze_axe", {
    description = S("An bronze axe"),
    inventory_image = "pmb_bronze_axe.png",
    tool_capabilities = {
        groupcaps = {
            choppy = {
                times = { 0.25 },
                uses = 128,
            },
        },
    },
    groups = { axe = 3 },
})

minetest.register_tool("pmb_tools:bronze_shovel", {
    description = S("An bronze shovel"),
    inventory_image = "pmb_bronze_shovel.png",
    tool_capabilities = {
        groupcaps = {
            crumbly = {
                times = { 0.225 },
                uses = 128,
            },
        },
    },
    groups = { shovel = 3 },
})

---------------------------------------------------
---------------------DIAMOND-----------------------
---------------------------------------------------

minetest.register_tool("pmb_tools:diamond_pickaxe", {
    description = S("A diamond pickaxe"),
    inventory_image = "pmb_diamond_pickaxe.png",
    tool_capabilities = {
        groupcaps = {
            cracky = {
                times = { .3, .5, .7 },
                uses = 1024,
            },
        },
    },
    groups = { pickaxe = 4 },
})

minetest.register_tool("pmb_tools:diamond_axe", {
    description = S("A diamond axe"),
    inventory_image = "pmb_diamond_axe.png",
    tool_capabilities = {
        groupcaps = {
            choppy = {
                times = { 0.175 },
                uses = 1024,
            },
        },
    },
    groups = { axe = 4 },
})

minetest.register_tool("pmb_tools:diamond_shovel", {
    description = S("A diamond shovel"),
    inventory_image = "pmb_diamond_shovel.png",
    tool_capabilities = {
        groupcaps = {
            crumbly = {
                times = { 0.125 },
                uses = 1024,
            },
        },
    },
    groups = { shovel = 4 },
})

dofile(mod_path .. DIR_DELIM .. "crafting.lua")
