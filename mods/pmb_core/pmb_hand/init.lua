local hand_groupcap = {
	oddly_breakable_by_hand = {
		times = { 0.7, 1.4, 3, 7 },
		uses = 0,
	}
}
if minetest.is_creative_enabled("") then
	hand_groupcap = {
		oddly_breakable_by_hand = {
			times = { 0.1, 0.1, 0.1, 0.1 },
			uses = 0,
		},
		cracky = {
			times = {0.1, 0.1, 0.1},
			uses = 0,
		},
	}
end

--  add hand tool
minetest.override_item("", {
	wield_scale = { x = 1, y = 1, z = 1 },
	tool_capabilities = {
		full_punch_interval = 0.5,
		max_drop_level = 0,
		groupcaps = hand_groupcap,
		damage_groups = { fleshy = 1 },
	}
})
