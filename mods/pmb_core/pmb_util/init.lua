local mod_name = minetest.get_current_modname()
local mod_path = minetest.get_modpath(mod_name)
local S = minetest.get_translator(mod_name)


pmb_util = {}


dofile(mod_path .. DIR_DELIM .. "scripts" .. DIR_DELIM .. "rotate_node.lua")
dofile(mod_path .. DIR_DELIM .. "scripts" .. DIR_DELIM .. "only_place_on.lua")
dofile(mod_path .. DIR_DELIM .. "scripts" .. DIR_DELIM .. "itemdrop.lua")
