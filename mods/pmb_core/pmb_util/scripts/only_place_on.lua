
function pmb_util.only_place_above(itemstack, placer, pointed_thing, groups)
  local on_node = minetest.get_node(vector.offset(pointed_thing.above, 0, -1, 0))
  for _, group in pairs(groups) do
    if minetest.get_item_group(on_node.name, group) ~= 0 then
      return minetest.item_place(itemstack, placer, pointed_thing)
    end
  end
  return itemstack
end