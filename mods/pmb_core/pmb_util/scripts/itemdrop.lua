local function drop(pos, items)
    for _, item in ipairs(items) do
        minetest.add_item(pos, item)
    end
end

function minetest.handle_node_drops(pos, drops, digger)
    local node = minetest.get_node(pos)
    local node_def = minetest.registered_nodes[node.name]

    if not digger then
        drop(pos, minetest.get_node_drops(node, ""))
        return
    end
    local tool = digger:get_wielded_item()

    drops = minetest.get_node_drops(node, tool:get_name())
    if minetest.get_item_group(tool:get_name(), "pickaxe") < minetest.get_item_group(node.name, "cracky") then
        drops = {}
        return
    end

    local inv = digger:get_inventory()
    if not inv then
        drop(pos, drops)
        return
    end

    if minetest.settings:get_bool("drop_item") then
        drop(pos, drops)
    else
        for _, item in ipairs(drops) do
            inv:add_item("main", item)
        end
    end
end
