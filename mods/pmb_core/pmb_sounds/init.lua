

pmb_sounds = {}

local snd = pmb_sounds


function pmb_sounds.default()
  return {
    footstep =  {name = "pmb_stone_step", gain = 0.1},
    dig =       {name = "pmb_stone_step", gain = 0.1},
    dug =       {name = "pmb_stone_step", gain = 0.1},
    place =     {name = "pmb_stone_step", gain = 0.1},}
end
function pmb_sounds.default_stone()
  return {
    footstep =  {name = "pmb_stone_step", gain = 0.3},
    dig =       {name = "pmb_stone_dig", gain = 1},
    dug =       {name = "pmb_stone_dug", gain = 1},
    place =     {name = "pmb_stone_place", gain = 1},}
end
function pmb_sounds.default_wood()
  return {
    footstep =  {name = "pmb_stone_step", gain = 0.3},
    dig =       {name = "pmb_stone_dig", gain = 1},
    dug =       {name = "pmb_stone_dug", gain = 1},
    place =     {name = "pmb_stone_place", gain = 1},}
end
function pmb_sounds.default_glass()
  return {
    footstep =  {name = "pmb_stone_step", gain = 0.1},
    dig =       {name = "pmb_stone_dig", gain = 1},
    dug =       {name = "pmb_stone_dug", gain = 1},
    place =     {name = "pmb_stone_place", gain = 1},}
end
function pmb_sounds.default_plant()
  return {
    footstep =  {name = "pmb_plant_dig", gain = 0.2, pitch = 1.1},
    dig =       {name = "pmb_plant_dig", gain = 0.4, pitch = 0.9},
    dug =       {name = "pmb_plant_dig", gain = 1},
    place =     {name = "pmb_plant_dig", gain = 1},}
end
function pmb_sounds.default_soil()
  return {
    footstep =  {name = "pmb_plant_dig", gain = 0.1},
    dig =       {name = "pmb_soil_place", gain = 0.6},
    dug =       {name = "pmb_soil_dug", gain = 1},
    place =     {name = "pmb_soil_place", gain = 1},}
end
function pmb_sounds.default_snow()
  return {
    footstep =  {name = "pmb_snow_step", gain = 0.7},
    dig =       {name = "pmb_soil_place", gain = 0.8},
    dug =       {name = "pmb_soil_dug", gain = 1},
    place =     {name = "pmb_snow_step_2", gain = 1},}
end