

-- this is intended to form a node update system, where if a node is updated by being dug, it notifies its neighbours in case they need to do something in response.

pmb_node_update = {}


pmb_node_update.update_functions = {}


local calls = 0
local call_limit = 500 -- per step
local propagate_limit = 15 -- number of nodes to propagate to

local function reset_calls(dtime)
  -- if calls ~= 0 then
  --   minetest.log(calls)
  -- end
  calls = 0
end

minetest.register_globalstep(reset_calls)

function pmb_node_update.register_on_node_update(name, func)
  pmb_node_update.update_functions[name] = func
end

local adjacent = {
  [0] = vector.new(0, 1, 0),
  [1] = vector.new(0, -1, 0),
  [2] = vector.new(1, 0, 0),
  [3] = vector.new(-1, 0, 0),
  [4] = vector.new(0, 0, 1),
  [5] = vector.new(0, 0, -1),
}

function pmb_node_update.update_node_propagate(pos, cause, user, count, delay)
  if not delay then delay = 0.1 end
  -- only allow a certain limit on total updates per server step
  calls = calls + 1
  if calls > call_limit then
    minetest.log("WARNING! TOO MANY NODE UPDATES ARE HAPPENING.")
    return false end

  -- only allow 15 recursions per update
  if count >= propagate_limit then return end

  local offset = 2 -- math.random(0, 5)
  for i=0, #adjacent do
    local p = adjacent[(i + offset) % 6]
    local v = vector.add(pos, p)
    if count == 0 then
      pmb_node_update.update_node(v, cause, user, count+1, delay)
    else
      minetest.after(delay, pmb_node_update.update_node, v, cause, user, count+1)
    end
  end
end

function pmb_node_update.update_node(pos, cause, user, count, delay)
  if count > propagate_limit then return false end

  local node = minetest.registered_nodes[(minetest.get_node(pos).name)]

  if node then
    local updated = false
    if node._on_node_update and node._on_node_update(pos, cause, user, count+1) then
      updated = true
    end
    -- go through the registered update funcs and if any of them return true, propogate the update
    for _, node_func in pairs(pmb_node_update.update_functions) do
      if node_func(pos, cause, user, count) then
        updated = true
      end
    end

    if updated then
      pmb_node_update.update_node_propagate(pos, cause, user, count+1, delay)
      return true
    end
  end
end



minetest.register_on_dignode(
  function(pos, oldnode, digger)
    pmb_node_update.update_node_propagate(pos, "dig", digger, 0)
  end
)
minetest.register_on_placenode(
  function(pos, oldnode, digger)
    pmb_node_update.update_node_propagate(pos, "place", digger, 0)
  end
)
minetest.register_on_punchnode(
  function(pos, node, puncher, pointed_thing)
    pmb_node_update.update_node(pos, "punch", puncher, 0)
  end
)


