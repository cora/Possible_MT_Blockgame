# Possible Minetest Blockgame

## LICENSES and copyleft
If a commit contains copyleft licensed material, or any licensed material that would require use to re-license this entire project under someone else's copyright, that commit will be reverted. Do not add anything that is not either: compatible with the MIT license, or able to be licensed without affecting the license of the project. Please license your major additions / modules under such a license as MIT or BSD which allows you to retain copyrights.

This is extremely important, as many games for minetest are GPL which forces modifications or derivitive works to re-license their entire work under GPL, which gives copyrights to the Free Software Foundation. We want to be free to do what we want with this project, and have ownership over it, and copyleft licenses threaten that and the copyrights of all contributors.

Free and open source. Free as in freedom, not free as in "free but you do as we tell you".

## The project scope

A game that:
- is a blockgame
- fun to play
- allows creativity
- allow many different play styles
- have optional challenges
- have resources that are obtained through varied methods

## Main Design Principles
### Sandbox
  - every part of the game should be at least mostly optional, have multiple ways to achieve the same thing, no arbitrary goals set by the devs
### Fun
  - design and mechanics should encourage behaviors in the player that are fun
  - don’t add things that encourage the player to do boring tasks or that encourage the game to be exploited in a boring way
  - exploiting the game in fun, fair ways = good
### Emergent
  - features should interact due to being independent and generalized instead of specific (content vs mechanics)
  - adding something that has interactions with two other things means we get very rich gameplay possibilities just from adding one thing, because those two things each interact with two other things and so on
### Fair
  - there should be an obvious cause > effect, rather than random inconveniences
  - don't punish the player
### Abstracted
  - over-simulation should probably be avoided unless *absolutely necessary* for the mechanic to work (i.e. redstone is simulated, but we don’t need to simulate air pressure so we know if the player is hot and needs to drink more water - that would be oversimulated)
  - arguably, having to drink water is also oversimulated
  - ask "does this make gameplay more interesting" and not from a developer standpoint but from a player's


## More Design Principles

1. features should be general features and interact with other features
  - instead of adding an arrow shooter node, add dispensers which can shoot anything that’s a projectile
  - to prevent you from shooting all projectiles, allow the projectile definition to choose whether it can be shot
2. specific mechanics that only have one use can be made a branching feature
  - if we have slimes in the game and they give slime balls, we add slime blocks which interact with other parts of the game (e.g. redstone)
3. the player should not be encouraged by the game to play in a boring or monotonous way
4. the player should not be forced to play in a varied way
5. mechanics that require the player to complete more tasks should be interrogated
  - this should not be arbitrary, it should interact with other parts of the game and for a reason
  - example, don't add crafting recipes just to slow the player down on the way to crafting something cool, instead, make the items needed be interactive within the game
6. the player should not be punished by the game for wanting to do something
  - obstacles are fine, but just "making things harder" for no reason adds nothing
